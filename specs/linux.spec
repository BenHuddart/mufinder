# -*- mode: python -*-

import os
import ase
import soprano
import mufinder

block_cipher = None

a = Analysis(['gui.py'],
             pathex=['.'],
             binaries=[],
             datas=[(os.path.join(ase.__path__[0],'spacegroup/spacegroup.dat'),'ase/spacegroup/'),(os.path.join(soprano.__path__[0],'data'),'soprano/data')],
             hiddenimports=['ase.io.cif','ase.gui.ag'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)

pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='MuFinder',
          icon=os.path.join(mufinder.__path__[0],'data/icons/mufinder_logo.gif'),
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True )

import shutil

shutil.copyfile(os.path.join(mufinder.__path__[0],'data/mufinder.ini'), '{0}/mufinder.ini'.format(DISTPATH))
shutil.copyfile(os.path.join(mufinder.__path__[0],'data/icons/mufinder_logo.gif'), '{0}/icons/mufinder_logo.gif'.format(DISTPATH))
shutil.copytree(os.path.join(mufinder.__path__[0],'data/templates'),os.path.join('{0}'.format(DISTPATH),'templates'))
