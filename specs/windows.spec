# -*- mode: python ; coding: utf-8 -*-
import sys, os
import ase
import soprano
import mufinder
sys.setrecursionlimit(5000)

block_cipher = None


a = Analysis(['gui.py'],
             pathex=['.'],
             binaries=[],
             datas=[(os.path.join(ase.__path__[0],'spacegroup\spacegroup.dat'),'ase\\spacegroup\\'),(os.path.join(soprano.__path__[0],'data'),'soprano\\data'),
			 (os.path.join(mufinder.__path__[0],'data\\icons\\mufinder_logo.ico'),'icons'),(os.path.join(mufinder.__path__[0],'data\\mufinder.ini'),'.'),
			 (os.path.join(mufinder.__path__[0],'data\\templates'),'templates')],
             hiddenimports=['ase.io.cif','ase.gui.ag'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='MuFinder',
          icon=os.path.join(mufinder.__path__[0],'data\\icons\\mufinder_logo.ico'),
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True)
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='MuFinder')
