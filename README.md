# MuFinder

A program to classify and analyse muon stopping sites.  This software is in open beta, we would be grateful for any bug reports or feature requests.

We recommend installing from the repository using pip. This can be done using the command\
`pip install git+https://gitlab.com/BenHuddart/mufinder.git`

Note that the muLFC library used by MuFinder needs to access numpy header files on installation. Hence if you are installing to a Python installation without numpy, you will need to install this first. This can be done using the following command:\
`pip install numpy`

Once installed, the MuFinder GUI can be launched by typing`mufinder` in the command line.

Executables for Windows and Linux can be accessed through the [Releases][Releases-link] page, which also includes a manual.

The files needed to work through the examples in the manual can be found in the 'examples' folder.

In all papers using MuFinder please cite: B. M. Huddart, A. Hernández-Melián, T. J. Hicken, M. Gomilšek, Z. Hawkhead, S. J. Clark, F. L. Pratt, and T. Lancaster, MuFinder: A program to determine and analyse muon stopping sites, [Comput. Phys. Commun. 280, 108488, (2022)][paper].

[Releases-link]: https://gitlab.com/BenHuddart/mufinder/-/releases
[paper]: https://doi.org/10.1016/j.cpc.2022.108488
