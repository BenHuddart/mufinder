import warnings

import ase.io
import numpy as np
import pytest

from mufinder.core.clustering.clustering import process_results
from mufinder.core.clustering.components import connected_components
from mufinder.core.clustering.displacements import calculate_radial_displacement
from mufinder.core.clustering.kmeans import kmeans_clustering
from mufinder.core.clustering.shift import add_sites, shift_sites

ATOM_PATH_SITES = [
    (
        "Fe/Fe.cif",
        "Fe/demo_results/",
        {
            "kmeans_num_clusters": 2,
            "connected_components_tol": 1.0,
            "connected_components_clusters": 1,
        },
    ),
]


@pytest.fixture(scope="module", params=ATOM_PATH_SITES)
def results(request, examples_path):
    input_path, results_path, meta = request.param

    with warnings.catch_warnings():
        # Ignore crystal system checking warning
        warnings.filterwarnings(
            "ignore", message="crystal system", category=UserWarning
        )

        input_struct = ase.io.read(examples_path / input_path)
        input_struct.info.update(meta)

    results = process_results(input_struct, examples_path / results_path, "H:mu")

    return (input_struct, results)


@pytest.mark.filterwarnings("ignore:The truth value of an empty array")
def test_kmeans(results):
    input_struct, results = results

    num_clusters = input_struct.info["kmeans_num_clusters"]
    resultsColl = results["collection"]

    clusters = kmeans_clustering(resultsColl, results["muon_label"], num_clusters)

    assert len(clusters) == num_clusters


def test_connected_components(results):
    input_struct, results = results

    tol = input_struct.info["connected_components_tol"]
    expected_clusters = input_struct.info["connected_components_clusters"]
    sites = results["sites"]
    clusters = connected_components(input_struct, sites, tol)

    assert len(clusters) == expected_clusters


def test_shift_sites(results):
    input_struct, results = results

    tol = input_struct.info["connected_components_tol"]
    clusters = connected_components(input_struct, results["sites"], tol)

    shifted = shift_sites(
        input_struct,
        results["muon_label"],
        results["sites"],
        clusters,
    )

    shifted_muons = shifted[shifted.symbols == results["muon_label"]]

    assert len(shifted_muons) == len(results["sites"])


def test_add_sites(results):
    input_struct, results = results
    added = add_sites(
        input_struct,
        results["muon_label"],
        results["sites"],
    )

    added_muons = added[added.symbols == results["muon_label"]]

    assert len(added_muons) == len(results["sites"])


def test_radial_displacement(results):
    input_struct, results = results
    output_struct = results["valid_structs"][0]
    muon_label = results["muon_label"]

    symbols, muon_dist, radial_dist = calculate_radial_displacement(
        input_struct, output_struct, muon_label
    )

    struct_symbols = set(input_struct.get_chemical_symbols())

    # Atom species are the same as in input_struct
    assert not struct_symbols.symmetric_difference(set(symbols))

    # There is a distance for each atom in  structure expect the muon
    assert len(radial_dist) == len(
        output_struct[np.char.not_equal(output_struct.symbols, muon_label)]
    )

    # There is the same number of muon and radial distances
    assert len(radial_dist) == len(muon_dist)

    # There is the same number of radial distances and symbols
    assert len(radial_dist) == len(symbols)
