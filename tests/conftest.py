from pathlib import Path

import pytest


@pytest.fixture(scope="session")
def examples_path():
    return Path(__file__).absolute().parents[1] / "examples"
