import warnings

import ase.io
import numpy as np
import pytest

import mufinder.core.sitegen.gen_sites as gen

ATOM_PATH_SITES = [
    ("Fe/Fe.cif", {"random_sites": 2, "closeto_element": "Fe"}),
    ("CuOSeO/CuOSeO.cif", {"random_sites": 30, "closeto_element": "Cu"}),
]


@pytest.fixture(params=ATOM_PATH_SITES)
def atoms(request, examples_path):
    atom_path, meta = request.param

    with warnings.catch_warnings():
        # Ignore crystal system checking warning
        warnings.filterwarnings(
            "ignore", message="crystal system", category=UserWarning
        )

        atoms = ase.io.read(str(examples_path / atom_path))
        atoms.info.update(meta)

        return atoms


@pytest.fixture
def random_sites(atoms):
    return gen.gen_random(atoms, minr=0.5, vdws=1.0, seed=0)


@pytest.fixture
def element_sites(atoms):
    return gen.gen_closeto(atoms, atoms.info["closeto_element"], vdws=1.0, seed=0)


@pytest.fixture
def clustered_sites(atoms, random_sites):
    return gen.gen_cluster(atoms, np.stack(random_sites))


def test_gen_random(atoms, random_sites):
    minr = 0.5
    vdws = 1.0

    assert len(random_sites) == atoms.info["random_sites"]

    _, D = ase.geometry.get_distances(
        atoms.cell.cartesian_positions(random_sites), cell=atoms.cell, pbc=True
    )

    np.fill_diagonal(D, np.inf)

    # Check that the sites are at least the minimum distance from each other
    assert (D.min(axis=0) > minr).all()

    _, D = ase.geometry.get_distances(
        atoms.get_positions(),
        atoms.cell.cartesian_positions(random_sites),
        cell=atoms.cell,
        pbc=True,
    )

    # Check that the sites are at least the minimum distance from the atoms
    assert (D.min(axis=0) > vdws).all()


def test_gen_closeto(atoms, element_sites):
    element = atoms.info["closeto_element"]
    distance = 1.0

    # Check that every atom has a muon site
    assert len(element_sites) == len(atoms[atoms.symbols == element])

    _, D = ase.geometry.get_distances(
        atoms[atoms.symbols == element].get_positions(),
        atoms.cell.cartesian_positions(element_sites),
        cell=atoms.cell,
        pbc=True,
    )

    # Check that the sites are the currect distance from the atoms
    assert np.allclose(D.min(axis=0), distance)


def test_gen_cluster(random_sites, clustered_sites):
    assert len(random_sites) == len(clustered_sites)
