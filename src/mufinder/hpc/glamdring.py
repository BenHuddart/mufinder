"""
Functions to use the Glamdring's addqueue job submission
utility
"""
def addqueue(jobname, num_cores, seed_name):

    # remove whitespace and fill with underscores
    comment = jobname.replace(' ','_').ljust(8,'_')

    command = '/bin/addqueue -c "{}" -n {} -q redwood -m 2 /mnt/redwoodfs/muon/castep.mpi {}'\
        .format(comment, num_cores, seed_name)

    return command