import math
import os

from mufinder.data import load_template

# TODO: Load script templates directory from config


def write_script(
    hpc_name, submission_system, dest, short, num_cores, jobname, cores_per_node, time
):

    # Get number of nodes from number of cores (using upside-down floor division)
    nodes = -(int(num_cores) // -int(cores_per_node))

    # Load script template text
    script_main = load_template(f"{hpc_name}.{submission_system}")

    var = {
        "short": short,
        "num_cores": num_cores,
        "jobname": jobname,
        "nodes": nodes,
        "time": time,
    }

    contents = script_main.format(**var)
    script = os.path.join(dest, "script.%s" % submission_system)

    with open(script, "w", newline="\n") as f:
        f.write(contents)
