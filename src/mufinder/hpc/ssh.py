import os
import posixpath
import shlex
import re

import paramiko

def check_connection(session):
    """
    This will check if the connection is still availlable.

    Return (bool) : True if it's still alive, False otherwise.
    """
    try:
        session.exec_command("ls", timeout=5)
        return True
    except Exception:
        return False


def create_session(hostname, username, password):
    session = paramiko.SSHClient()  # Open the session
    session.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    session.connect(hostname=hostname, username=username, password=password)
    return session


def send_command(session, cmd):

    stdin, stdout, stderr = session.exec_command(cmd)
    return "".join(stdout.readlines()), "".join(stderr.readlines())


def sftp(session, localpath, remotepath, recursive=False):
    sftp = session.open_sftp()

    if recursive:
        os.chdir(os.path.split(localpath)[0])
        parent = os.path.split(localpath)[1]
        for walker in os.walk(parent):
            try:
                sftp.mkdir(posixpath.join(remotepath, walker[0]))
            except Exception:
                pass
            for file in walker[2]:
                sftp.put(
                    posixpath.join(walker[0], file),
                    posixpath.join(remotepath, walker[0], file),
                )

    else:
        sftp.put(localpath, remotepath)

    sftp.close()


def pull(session, remotepath, localpath):
    sftp = session.open_sftp()
    sftp.get(remotepath, localpath)
    sftp.close()


def submit_job(session, submission_sys, working_dir, site_no, custom_submit):

    if submission_sys == "addqueue":
        cmd = "cd %s/site%i/; %s;" % (working_dir, site_no, custom_submit)
        output, error = send_command(session, cmd)
        id_sdb = re.split('-|\.', output)
        job_id = [int(s) for s in id_sdb if s.isdigit()][0]

        return job_id

    if submission_sys == "slurm":
        scriptname = "script.slurm"
        sub_comm = "sbatch %s" % scriptname

    elif submission_sys == "pbs":
        scriptname = "script.pbs"
        sub_comm = "qsub %s" % scriptname

    else:
        scriptname = "script.custom"
        sub_comm = "%s %s" % (custom_submit, scriptname)

    cmd = "cd %s/site%i/; %s;" % (working_dir, site_no, sub_comm)
    output, error = send_command(session, cmd)

    if submission_sys == "pbs":
        id_sdb = output.readlines().split()[-1]
        job_id = [int(s) for s in id_sdb.split(".") if s.isdigit()][0]
    else:
        job_id = [int(s) for s in output.split() if s.isdigit()][0]

    return job_id


def copy_queue(submission_sys, session, username, custom_queue_check):
    # get string resulting from check of the queue

    if submission_sys == "slurm":
        cmd = "squeue -u %s" % username

    elif submission_sys == "pbs":
        cmd = "qstat -u %s" % username

    else:
        cmd = "%s -u %s" % (custom_queue_check, username)

    queue_str, session = send_command(session, cmd)

    return queue_str


def search_queue(job_id, queue_str, submission_sys):

    id_plus = str(job_id)

    if id_plus in queue_str:
        inqueue = True
        matched_line = [line for line in queue_str.split("\n") if id_plus in line]
        if submission_sys == "pbs":
            job_status = shlex.split(matched_line[0])[9]
        else:
            job_status = shlex.split(matched_line[0])[4]

        if job_status == "PD" or job_status == "Q":
            status = "waiting"
        elif job_status == "R":
            status = "running"
    else:
        inqueue = False
        status = "null"

    return inqueue, status
