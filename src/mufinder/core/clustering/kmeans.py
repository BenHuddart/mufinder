"""Muon site clustering using k-means."""

from operator import itemgetter

from soprano.analyse.phylogen import Gene, PhylogenCluster


def kmeans_clustering(resultsColl, muon_label, num_clusters):
    """Cluster soprano.AtomsCollection structurs using k-means method.

    The energy of the structure and the bond order parameters around the
    muon are used as features.
    """
    geneE = Gene("energy")
    geneBOrd = Gene("bond_order_pars", params={"s1": muon_label})

    pylogen = PhylogenCluster(resultsColl, genes=[geneBOrd, geneE])

    inds, composition = pylogen.get_kmeans_clusters(num_clusters)
    clusters = [cluster.tolist() for cluster in composition]

    return list(sorted(clusters, key=itemgetter(0)))
