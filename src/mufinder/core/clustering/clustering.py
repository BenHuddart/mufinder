import logging
import math
import mmap
import os
import re
import time
from pathlib import Path

import numpy as np
from ase import io
from ase.calculators.singlepoint import SinglePointCalculator
from ase.data import atomic_numbers
from soprano.collection import AtomsCollection

from mufinder.core.clustering.components import connected_components
from mufinder.core.clustering.displacements import max_displacement
from mufinder.core.clustering.kmeans import kmeans_clustering
from mufinder.core.clustering.shift import add_sites, shift_sites


class MuonLabelError(Exception):
    pass


class CastepEnergyError(Exception):
    pass


def get_muon_label(path):
    """Automatically detect the label used for the muon in a file."""
    with open(path) as f:
        contents = f.read()

        if "H:mu" in contents:
            return "H:mu"

        if "H " in contents:
            return "H"

    return None


def relabel_muon(output_files, old_label, new_label):
    """Write new set of output files replacing old_label with new_label."""
    renamed_files = []

    for path in output_files:
        renamed = os.path.join(os.path.dirname(path), "tmp_" + os.path.basename(path))

        with open(path, "r") as f1:
            with open(renamed, "w") as f2:
                for line in f1:
                    f2.write(line.replace(old_label, new_label))

        renamed_files.append(renamed)

    return renamed_files


def castep_final_energy(path):
    energy = None
    needles = [
        b"NB est. 0K energy",
        b"NB dispersion corrected est. 0K energy",
        b"Final energy, E",
    ]

    with open(path) as f:
        with mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ) as s:
            for needle in needles:
                start = s.rfind(needle)

                if start == -1:
                    continue

                end = s.find(b"\n", start)
                energy = s[start:end].decode("utf-8")
                break

    if energy is not None:
        m = re.search(r"(-?\d+\.?\d+)\s+eV", energy)
        if m:
            return float(m.group(1))
        else:
            raise CastepEnergyError(energy)
    else:
        raise CastepEnergyError(energy)


def read_results(cell_files, castep_files, label="", alt_label="X", logger=None):
    logger = logging.getLogger(logger)

    # Get label for muon if not specified by user
    if label == "":
        label = get_muon_label(cell_files[0])
        if not label:
            logger.error(
                "Unable to detect muon label in %s. Aborting...", cell_files[0]
            )
            raise MuonLabelError

    # ASE cannot properly interpret H:mu, here we relabel the muon to fix this
    if label == "H:mu":
        cell_files_tmp = relabel_muon(cell_files, label, alt_label)

        results = read_results(
            cell_files_tmp, castep_files, label=alt_label, logger=logger.name
        )

        # delete temporary files
        for file in cell_files_tmp:
            os.remove(file)

        return results

    logger.info("Loading structures...")

    atom_list = []
    energy_list = []
    index_list = []

    for cell_path, energy_path in zip(cell_files, castep_files):
        index = int(re.search(r"_([0-9]+)-out\.cell", Path(cell_path).name).group(1))
        cell = io.read(cell_path, calculator_args={"keyword_tolerance": 3})

        try:
            energy = castep_final_energy(energy_path)
        except (CastepEnergyError, FileNotFoundError):
            logger.warn("Unable to read energy from %s", energy_path)
            continue

        cell.calc = SinglePointCalculator(cell, energy=energy)

        atom_list.append(cell)
        index_list.append(index)
        energy_list.append(energy)

    resultsColl = AtomsCollection(atom_list)

    logger.info("Loaded %s structures.", len(resultsColl))

    # Extract the list of energies and append it to the collection
    energy_array = np.array(energy_list)
    resultsColl.set_array("energy", energy_array)

    index_array = np.array(index_list)
    resultsColl.set_array("index", index_array)

    resultsColl = resultsColl.sorted_byarray("energy")  # Sort by energy

    return resultsColl, label


def process_results(input_struct, out_path, muon_label, max_disp=None, logger=None):
    logger = logging.getLogger(logger)

    # Loading CASTEP output files
    cell_files = list(Path(out_path).glob("*-out.cell"))
    castep_files = [
        path.with_name(re.sub(r"-out\.cell$", ".castep", path.name))
        for path in cell_files
    ]

    # Load collection of results
    try:
        resultsColl, new_label = read_results(
            cell_files,
            castep_files,
            label=muon_label,
            logger=logger.name + ".read_results",
        )
    except ValueError:
        return

    # Extract fractional coordinates of muon sites and translate these to
    # coordinates in the conventional cell if a supercell has been used
    muon_sites = []
    muon_sites_supercell = []
    valid_structs = []

    input_inv = np.linalg.inv(input_struct.get_cell())
    muon_number = atomic_numbers[new_label]

    for result_struct in resultsColl.structures:
        if max_disp is not None:
            struct_disp = max_displacement(input_struct, result_struct)
        else:
            struct_disp = 0.0

        scell_scale = result_struct.get_cell() @ input_inv

        ind = np.flatnonzero(result_struct.numbers == muon_number)
        muon_pos = ind and result_struct.get_scaled_positions()[ind]

        # check for 'broken' output files and maximum allowed displacement
        if max_disp is None or struct_disp <= max_disp:
            valid_structs.append(result_struct)
            muon_sites_supercell.append(muon_pos[0])
            muon_sites.append([math.modf(x)[0] for x in (muon_pos[0] @ scell_scale)])

    # TODO: Add error message when no muons are found
    return {
        "muon_label": new_label,
        "valid_structs": valid_structs,
        "collection": resultsColl,
        "sites": muon_sites,
        "supercell_sites": muon_sites_supercell,
    }


def cluster(
    algorithm,
    shift,
    muon_label,
    params,
    out_path,
    aout,
    use_max_disp,
    max_disp,
    logger=None,
):

    logger = logging.getLogger(logger)
    start_time = time.time()

    results = process_results(
        aout.input_struct,
        out_path,
        muon_label,
        max_disp=(max_disp if use_max_disp else None),
        logger=f"{logger.name}.process_results",
    )

    aout.new_label = results["muon_label"]
    aout.valid_struct = results["valid_structs"]
    aout.muon_sites = results["sites"]
    aout.muon_sites_supercell = results["supercell_sites"]

    if algorithm == "connected components":
        aout.clust_composition = connected_components(
            aout.input_struct,
            aout.muon_sites,
            float(params),
            logger=f"{logger.name}.connected_components",
        )
    elif algorithm == "k-means":
        aout.clust_composition = kmeans_clustering(
            results["collection"], results["muon_label"], int(params)
        )

    logger.info("Clusters are: %s", aout.clust_composition)

    if shift:
        logger.info(
            "Shifting muon positions to occupy smaller subspace of the unit cell..."
        )

        aout.shifted = shift_sites(
            aout.input_struct,
            aout.new_label,
            aout.muon_sites,
            aout.clust_composition,
        )
    else:
        aout.shifted = add_sites(
            aout.input_struct,
            aout.new_label,
            aout.muon_sites,
        )

    logger.info("Done.")

    time_taken = time.time() - start_time
    logger.info("Finished in %s seconds.", round(time_taken, 2))

def single_cluster(aout, cluster_no):
    index = int(cluster_no) - 1
    members = np.array([int(i) for i in aout.clust_composition[index]])
    sites_in_cluster = []
    for j in members:
        sites_in_cluster.append(aout.muon_sites[j])

    cluster = add_sites(
        aout.input_struct,
        aout.new_label,
        np.array(sites_in_cluster),
    )

    return cluster