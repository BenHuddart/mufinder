import numpy as np
from ase.atoms import Atoms

from mufinder.core.sitegen.gen_sites import gen_sym_equiv


def add_sites(input_struct, new_label, muon_sites):
    all_muons = Atoms(
        symbols=[new_label] * len(muon_sites),
        scaled_positions=muon_sites,
        cell=input_struct.cell,
    )
    all_muons += input_struct

    return all_muons


def shift_sites(input_struct, new_label, muon_sites, clusters):
    shifted_positions = []

    for cluster in clusters:
        # find member of each subgraph that is closest to the origin,
        # but still inside cell
        dist_origin = []
        sub_muon_sites = [muon_sites[p] for p in cluster]

        for site in sub_muon_sites:
            replica_i = gen_sym_equiv(site, input_struct, ["O"])
            min_dist = min(
                np.linalg.norm(single_mu.position)
                for single_mu in replica_i
                if all(x >= 0 for x in single_mu.position)
            )
            dist_origin.append(min_dist)

        origin_index = np.argmin(dist_origin)

        # translate each member of subgraph to be closest to the
        # member that is closest to the origin

        replica_1 = gen_sym_equiv(muon_sites[origin_index], input_struct, ["H"])

        distances = []
        muon_cart_pos = []

        for single_mu in replica_1:
            muon_cart = single_mu.position
            if all(coord >= 0 for coord in muon_cart):
                muon_cart_pos.append(muon_cart)
                distances.append(np.linalg.norm(muon_cart))

        muon_frac_pos = (
            np.linalg.inv(replica_1.cell) @ muon_cart_pos[np.argmin(distances)]
        )
        replica_1 = gen_sym_equiv(muon_frac_pos, input_struct, ["H"])
        sub_muon_sites = [muon_sites[p] for p in cluster]

        for site_i in sub_muon_sites:
            replica_i = gen_sym_equiv(site_i, input_struct, ["O"])
            combined = replica_i + replica_1
            atoms1 = [
                x
                for x in range(len(combined.get_atomic_numbers()))
                if combined.get_chemical_symbols()[x] == "O"
            ]
            atoms2 = [
                y
                for y in range(len(combined.get_atomic_numbers()))
                if combined.get_chemical_symbols()[y] == "H"
            ]
            distances = combined.get_distances(atoms1, atoms2[0], mic=False)
            index = np.argmin(distances)

            shifted_positions.append(replica_i.get_scaled_positions()[index])

    shifted = Atoms(
        symbols=[new_label] * len(shifted_positions), 
        scaled_positions=shifted_positions,
        cell=input_struct.cell,
    )

    # get order that sites are added to 'shifted'
    site_order = []

    for cluster in clusters:
        for p in cluster:
            site_order.append(p)

    unsrt_pos = shifted.get_positions()
    sort_pos = [x for _, x in sorted(zip(site_order, unsrt_pos))]
    shifted.set_positions(sort_pos)
    shifted += input_struct  # embed muons in structure

    return shifted
