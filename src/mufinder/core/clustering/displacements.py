import numpy as np
from ase.build.supercells import make_supercell
from ase.data.colors import jmol_colors
from ase.geometry import find_mic, get_distances
from ase.symbols import symbols2numbers
from matplotlib import pyplot as plt
from matplotlib.lines import Line2D
from scipy.optimize import linear_sum_assignment


def make_supercell_like(input_struct, target_struct):
    combined_cell = target_struct.get_cell() @ np.linalg.inv(input_struct.get_cell())
    combined_cell_int = np.round(combined_cell).astype(int)

    output_struct = make_supercell(input_struct, combined_cell_int)
    return output_struct


def projected_distance(a, b, axis=1):
    """Projected length of the first vector along the second."""
    return (a * b).sum(axis=axis) / np.linalg.norm(b, axis=axis)


def displacement_matrix(initial_struct, final_struct, fill_value=np.inf):
    """Calculate distance between atoms of the same species."""
    output = np.full((len(initial_struct), len(final_struct)), fill_value)

    initial_symbols = initial_struct.get_chemical_symbols()
    final_symbols = final_struct.get_chemical_symbols()

    symbols = set(initial_symbols) | set(final_symbols)

    for sym in symbols:
        initial_ind = np.char.equal(initial_symbols, sym)
        initial_positions = initial_struct.positions[initial_ind]

        final_ind = np.char.equal(final_symbols, sym)
        final_positions = final_struct.positions[final_ind]

        if initial_positions.size == 0 or final_positions.size == 0:
            continue

        _, D = get_distances(
            initial_positions, final_positions, cell=initial_struct.cell, pbc=True
        )

        output[np.ix_(initial_ind, final_ind)] = D

    return output


def calculate_radial_displacement(input_struct, final_struct, muon_label):
    initial_struct = make_supercell_like(input_struct, final_struct)

    # Calculate distances between atoms of same species
    displ_mat = displacement_matrix(initial_struct, final_struct)
    # Assign atoms between two structures to minimize displacement
    row_ind, col_ind = linear_sum_assignment(displ_mat)

    # Calculate vectors and distances from atoms to muon
    muon_ind = final_struct.get_atomic_numbers() == symbols2numbers(muon_label)
    muon_position = final_struct.positions[muon_ind]
    muon_vectors, muon_distances = get_distances(
        muon_position, initial_struct.positions, cell=initial_struct.cell, pbc=True
    )

    # Reorder distances using linear sum assignment
    muon_distances = muon_distances[:, row_ind]
    # Use minimal distance from muon to each atom
    muon_min_ind = muon_distances.argmin(axis=0)
    muon_dist = np.take_along_axis(muon_distances, muon_min_ind[None, :], 0).ravel()

    # Project atom displacement vector along vector between muon annd atom
    atom_disp, _ = find_mic(
        final_struct.positions[col_ind] - initial_struct.positions[row_ind],
        initial_struct.cell,
        pbc=True,
    )
    muon_disp = muon_vectors[muon_min_ind, row_ind]
    radial_dist = projected_distance(atom_disp, muon_disp)

    symbols = initial_struct[row_ind].get_chemical_symbols()

    return symbols, muon_dist, radial_dist


def plot_radial_displacement(input_struct, structure, muon_label):
    symbols, muon_dist, radial_dist = calculate_radial_displacement(
        input_struct, structure, muon_label
    )

    symbol_colors = np.array([jmol_colors[symbols2numbers(sym)] for sym in symbols])

    legend_labels = sorted(set(symbols), key=symbols2numbers)
    legend_colors = [jmol_colors[symbols2numbers(sym)] for sym in legend_labels]

    legend_elements = [
        Line2D(
            [0],
            [0],
            marker="o",
            color="w",
            label=label,
            markerfacecolor=color,
            markeredgecolor="black",
        )
        for label, color in zip(legend_labels, legend_colors)
    ]

    plt.figure()
    plt.scatter(muon_dist, radial_dist, c=symbol_colors, edgecolor="black")
    plt.ylabel("Radial displacement (Å)")
    plt.xlabel("Distance from muon site (Å)")
    plt.legend(handles=legend_elements)

    plt.show()


def max_displacement(input_struct, final_struct):
    initial_struct = make_supercell_like(input_struct, final_struct)

    displ_mat = displacement_matrix(initial_struct, final_struct)
    row_ind, col_ind = linear_sum_assignment(displ_mat)

    return np.max(displ_mat[row_ind, col_ind])
