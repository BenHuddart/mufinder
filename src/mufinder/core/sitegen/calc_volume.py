import logging
#import time

import numpy as np
import spglib as spg
from ase.cell import Cell
from ase.geometry import get_distances
from ase.spacegroup import Spacegroup, crystal

def merge_intervals(t_intervals):
    """Sort and merge overlapping intervals on a line"""
    # Similar to in place algorithm from https://www.geeksforgeeks.org/merging-intervals/

    if t_intervals.size==0:
        return t_intervals

    # Sort intervals by their starts
    t_intervals = t_intervals.tolist()
    t_intervals.sort()
    t_intervals = np.array(t_intervals)

    # Merge intervals
    i = 0
    for j in range(1, len(t_intervals)):
        if t_intervals[i, 1]>=t_intervals[j, 0]: # Does interval j overlap with the last merged interval i?
            if t_intervals[i, 1]<t_intervals[j, 1]: # Does interval j extend it?
                t_intervals[i, 1] = t_intervals[j, 1] # Extend interval i by interval j
        else:
            i += 1
            t_intervals[i] = t_intervals[j] # Add new interval

    # Return the merged intervals
    return t_intervals[:i+1]

def measure_intervals(t_intervals):
    """Total signed length of all intervals with no account for overlaps (positive if start<end)"""
    return np.sum(int[1]-int[0] for int in t_intervals)

def intersect_line_sphere(line_origin, line_dir, sphere_centre, sphere_r=1.0):
    """Find line-sphere intersection centre and interval in terms of the fraction of line_dir"""
    line_dir_norm = np.linalg.norm(line_dir)
    line_dir_unit = line_dir / line_dir_norm

    sphere_centre = sphere_centre - line_origin # Move the sphere centre as if the line passes through the origin
    r_perp = np.linalg.norm(np.cross(line_dir_unit, sphere_centre))
    if r_perp>sphere_r:
        t_interval = np.array([])
    else:
        t_centre = np.dot(line_dir_unit, sphere_centre) / line_dir_norm
        if r_perp==sphere_r:
            t_interval = np.array([t_centre, t_centre])
        else:
            t_diff = np.sqrt(sphere_r*sphere_r - r_perp*r_perp) / line_dir_norm
            t_interval = np.array([t_centre - t_diff, t_centre + t_diff])           

    return t_interval

def intersect_line_sphere_periodic(line_origin, line_dir, sphere_centre, sphere_r, periodic_vec1, periodic_vec2):
    """Find line-sphere intersection intervals in terms of the fraction of line_dir within the unit-cell [0, 1) interval, for a periodic 3D lattice of spheres with basis vectors: line_dir, periodic_vec1, and periodic_vec2"""
    # Bypass: are we requesting line intersections with multiple spheres?
    if sphere_centre.ndim>=2:
        if np.isscalar(sphere_r):
            sphere_r = np.full(len(sphere_centre), sphere_r)
        
        t_intervals = np.empty(shape=(0, 2)) #np.array([[]])
        for i in range(len(sphere_centre)):
            t_intervals = np.append(t_intervals, intersect_line_sphere_periodic(line_origin, line_dir, sphere_centre[i], sphere_r[i], periodic_vec1, periodic_vec2), axis=0)
        t_intervals = merge_intervals(t_intervals)

        return t_intervals

    def append_to_t_intervals_periodic(new_t_interval):
        if new_t_interval.size==0:
            #return 0
            return False
        else:
            nonlocal t_intervals
            #new_t_interval.sort() # Safety
            new_t_interval = new_t_interval - np.floor(new_t_interval[0]) # Shift its start to [0, 1)
            t_intervals = np.append(t_intervals, [new_t_interval], axis=0)
            
            new_t_interval -= 1      # Shift its start to [-1, 0)
            if new_t_interval[1]>=0: # Does its end overlap with [0, 1)?
                t_intervals = np.append(t_intervals, [new_t_interval], axis=0)
            #    return 2
            #else:
            #    return 1
            return True

    # Find periodic image of the sphere centre "closest" (in fractional coordinates) to line_origin and recentre the sphere there
    cell = Cell([periodic_vec1, periodic_vec2, line_dir])
    sphere_centre = line_origin + cell.cartesian_positions(np.mod(cell.scaled_positions(sphere_centre - line_origin) + 0.5, 1.0) - 0.5)

    # Find first intersection
    t_intervals = np.empty(shape=(0, 2)) #np.array([[]])
    append_to_t_intervals_periodic(intersect_line_sphere(line_origin, line_dir, sphere_centre, sphere_r))

    # Search for new intersections in expanding shells of periodic_vec1 & periodic_vec2 displacements around the recentred sphere until none are found anymore
    shell = 0
    changed = True
    while changed:
        shell += 1
        changed = False
        for i in range(-shell, shell+1):
            changed = changed or append_to_t_intervals_periodic(intersect_line_sphere(line_origin, line_dir, sphere_centre + shell*periodic_vec1 + i*periodic_vec2, sphere_r))
            changed = changed or append_to_t_intervals_periodic(intersect_line_sphere(line_origin, line_dir, sphere_centre - shell*periodic_vec1 + i*periodic_vec2, sphere_r))
        for i in range(-shell+1, shell):
            changed = changed or append_to_t_intervals_periodic(intersect_line_sphere(line_origin, line_dir, sphere_centre + i*periodic_vec1 + shell*periodic_vec2, sphere_r))
            changed = changed or append_to_t_intervals_periodic(intersect_line_sphere(line_origin, line_dir, sphere_centre + i*periodic_vec1 - shell*periodic_vec2, sphere_r))

    # Merge overlapping intervals
    t_intervals = merge_intervals(t_intervals)

    # Limit the merged intervals to range [0, 1)
    if t_intervals.size>0:
        if t_intervals[0, 0]<0:
            t_intervals[0, 0] = 0
        if t_intervals[-1, 1]>1:
            t_intervals[-1, 1] = 1
    
    return t_intervals

def measure_intersection_line_sphere_periodic(line_origin, line_dir, sphere_centre, sphere_r, periodic_vec1, periodic_vec2):
    """Find total length of line-sphere intersection intervals in terms of the fraction of line_dir within the unit-cell [0, 1) interval, for a periodic 3D lattice of spheres with basis vectors: line_dir, periodic_vec1, and periodic_vec2"""
    return measure_intervals(intersect_line_sphere_periodic(line_origin, line_dir, sphere_centre, sphere_r, periodic_vec1, periodic_vec2))

def measure_occupied_volume_fraction(line_origin_n1, line_origin_n2, line_dir, sphere_centre, sphere_r, periodic_vec1, periodic_vec2):
    """Approximate fraction of unit-cell volume inside of atoms with fixed radii on a line_origin_n1 x line_origin_n2 grid"""
    d_periodic_vec1 = periodic_vec1 / line_origin_n1
    d_periodic_vec2 = periodic_vec2 / line_origin_n2

    vol_occ_frac = 0
    for i in range(line_origin_n1):
        for j in range (line_origin_n2):
            vol_occ_frac += measure_intersection_line_sphere_periodic(i*d_periodic_vec1 + j*d_periodic_vec2, line_dir, sphere_centre, sphere_r, periodic_vec1, periodic_vec2)
    vol_occ_frac = vol_occ_frac / (line_origin_n1*line_origin_n2)

    #vol_free_frac = 1 - vol_occ_frac
    return vol_occ_frac

def sphere_volume(r=1.0):
    """3D sphere volume"""
    return 4 * np.pi * (r*r*r) / 3

def two_same_spheres_volume(d, r=1.0):
    """Volume of two, possibly overlapping, spheres of same radii"""
    if d<=0:
        vol_union = sphere_volume(r)
    else:
        vol_union = 2*sphere_volume(r)
        if d<2*r:
            vol_intersection = np.pi * (4*r + d) * (2*r - d)*(2*r - d) / 12 # https://mathworld.wolfram.com/Sphere-SphereIntersection.html , https://en.wikipedia.org/wiki/Spherical_cap
            vol_union = vol_union - vol_intersection

    return vol_union

def two_spheres_volume(d, r1=1.0, r2=1.0):
    """Volume of two, possibly overlapping, spheres of different radii"""
    if r1==r2:
        return two_same_spheres_volume(d, r1) # Faster

    r_diff = r1 - r2
    if d<=np.abs(r_diff):
        vol_union = sphere_volume(max(r1, r2))
    else:
        vol_union = sphere_volume(r1) + sphere_volume(r2)
        r_sum = r1 + r2
        if d<r_sum:
            vol_intersection = np.pi * (r_sum - d)*(r_sum - d) * (d*d + 2*d*r_sum - 3*r_diff*r_diff) / (12*d) # https://mathworld.wolfram.com/Sphere-SphereIntersection.html , https://en.wikipedia.org/wiki/Spherical_cap
            vol_union = vol_union - vol_intersection

    return vol_union

def get_isolated_atom_pos(atom_pos, cell, pbc, r=1.0):
    """Split atoms into isolated ones (no other atoms within their radius) and those that overlap"""
    _, atom_pos_dists = get_distances(
        atom_pos, atom_pos, cell, pbc
    )
    atom_pos_neigh = (atom_pos_dists<2*r)
    atom_pos_neigh_count = np.count_nonzero(atom_pos_neigh, axis=0)

    # Single isolated atoms    
    #atom_pos_isolated    = np.array([atom_pos[i] for i in range(len(atom_pos)) if atom_pos_neigh_count[i]<=1])
    num_isolated = np.count_nonzero(atom_pos_neigh_count<=1, axis=0)

    # Isolated pairs of atoms
    #atom_pos_pairs       = np.array([atom_pos[i] for i in range(len(atom_pos)) if atom_pos_neigh_count[i]==2])
    ind_pairs = [[[i, j] for j in range(i+1, len(atom_pos)) if atom_pos_neigh[i, j]!=0] for i in range(len(atom_pos)) if atom_pos_neigh_count[i]==2]
    if len(ind_pairs)==0:
        dist_pairs = np.array([])
    else:
        ind_pairs = [x[0] for x in ind_pairs if x!=[]]
        dist_pairs = np.array([atom_pos_dists[x[0], x[1]] for x in ind_pairs])

    # Overlapping atoms in clusters of 3 or more
    atom_pos_higher_overlapping = np.array([atom_pos[i] for i in range(len(atom_pos)) if atom_pos_neigh_count[i]>2])

    #atom_pos_isolated = np.array([]) ##
    #atom_pos_pairs = np.array([]) ##
    #atom_pos_higher_overlapping = atom_pos ##
    #return atom_pos_isolated, atom_pos_pairs, atom_pos_higher_overlapping #    
    return num_isolated, dist_pairs, atom_pos_higher_overlapping #

def calc_free_volume(struct, r=1.0, logger=None, atom_pos=None, rel_tol=1e-2, max_n=100, verbose=False):#verbose=True):
    """Calculate unit-cell volume outside of atoms with fixed radii"""
    logger = logging.getLogger(logger)
    
    vol_cell = struct.get_volume()
    if r<=0:
        return vol_cell

    # Get atomic positions, if they are not overriden in the arguments, and cell parameters 
    if atom_pos is None:
        atom_pos = struct.get_positions()
    if atom_pos.size==0:
        return vol_cell
    cell = struct.get_cell()

    # Explicitly calculate volumes of isolated atoms and remove them from raytracing
    #tic = time.perf_counter()
#    atom_pos_isolated, atom_pos_doubles, atom_pos = get_isolated_atom_pos(atom_pos, struct.cell, struct.pbc, r)
    num_isolated, dist_pairs, atom_pos = get_isolated_atom_pos(atom_pos, struct.cell, struct.pbc, r)
    #toc = time.perf_counter()
    #logger.info(f"Elapsed time is {toc-tic:0.4f} seconds.") #

    #tic = time.perf_counter()
    vol_free = vol_cell - num_isolated * sphere_volume(r)
    if dist_pairs.size!=0:
        vol_free = vol_free - np.sum([two_same_spheres_volume(d, r) for d in dist_pairs])
    #toc = time.perf_counter()
    #logger.info(f"Elapsed time is {toc-tic:0.4f} seconds.") #

    # Treat the rest by raytracing, if there are any non-isolated atoms still left
    if atom_pos.size==0:
        if verbose:
            logger.info(f"Treated {num_isolated} single atoms, {2*len(dist_pairs)} atoms in pairs, raytracing {len(atom_pos)} highly intersecting atoms.") #
        return vol_free

    # Find unit cell face with the lowest area to raytrace lines from it
    cell_areas = np.linalg.norm(np.array([np.cross(cell[i - 2], cell[i - 1]) for i in range(3)]), axis=1) # Copy-paste of "Cell.areas" from newer ASE code: https://wiki.fysik.dtu.dk/ase/_modules/ase/cell.html#Cell.areas (code), https://wiki.fysik.dtu.dk/ase/ase/cell.html#ase.cell.Cell.areas (documentation)
    line_dir_ind = np.argmin(cell_areas)
    
    # Get raytracing and linearly-independent periodic directions 
    line_dir = cell[line_dir_ind]
    periodic_vec1 = cell[line_dir_ind-2]
    periodic_vec2 = cell[line_dir_ind-1]

    # Components of linearly-independent periodic directions orthogonal to the raytracing direction
    line_dir_unit = line_dir / np.linalg.norm(line_dir)
    periodic_vec1_perp_norm = np.linalg.norm(periodic_vec1 - line_dir_unit * np.dot(line_dir_unit, periodic_vec1))
    periodic_vec2_perp_norm = np.linalg.norm(periodic_vec2 - line_dir_unit * np.dot(line_dir_unit, periodic_vec2))
    
    ## Estimate the needed grid density to sample each sphere with a roughly "supersample x supersample" grid of rays
    #line_origin_n1 = np.ceil(supersample * periodic_vec1_perp_norm / r).astype(int)
    #line_origin_n2 = #np.ceil(supersample * periodic_vec2_perp_norm / r).astype(int)
    # Estimate the needed grid to sample the unit cell to an accuracy of ~rel_tol relative to the whole unit cell volume
    f = periodic_vec2_perp_norm / periodic_vec1_perp_norm
    line_origin_n1 = np.sqrt((1/rel_tol)/f)
    line_origin_n2 = np.ceil(f*line_origin_n1).astype(int)
    line_origin_n1 = np.ceil(line_origin_n1).astype(int)
    if max_n>0:
        line_origin_n1 = min(line_origin_n1, max_n)
        line_origin_n2 = min(line_origin_n2, max_n)

    # Calculate the free volume by subtracting occupied volume estimated by raytracing
    #logger.info(f"Estimating free cell volume using a {line_origin_n1} x {line_origin_n2} grid with rays along direction {line_dir_ind+1}.") #
    if verbose:
        logger.info(f"Treated {num_isolated} single atoms, {2*len(dist_pairs)} atoms in pairs, raytracing {len(atom_pos)} highly intersecting atoms with {line_origin_n1*line_origin_n2} rays.") #
    #logger.info(f"Raytracing volume occupied by {len(atom_pos)} atoms using {line_origin_n1*line_origin_n2} rays.") #
    #tic = time.perf_counter()
    vol_occ_frac = measure_occupied_volume_fraction(line_origin_n1, line_origin_n2, line_dir, atom_pos, r, periodic_vec1, periodic_vec2)
    vol_free = vol_free - vol_occ_frac * vol_cell
    #toc = time.perf_counter()
    #logger.info(f"Elapsed time is {toc-tic:0.4f} seconds.") #

    return vol_free

def get_all_muon_sites(struct, sites, bool_symmetrize=True, bool_Cartesian=True):
    """..."""
    if len(sites)==0:
        return np.array([])
    
    if bool_symmetrize:
        spg_cell = (
            struct.get_cell(),
            struct.get_scaled_positions(),
            struct.get_atomic_numbers(),
        )
        sg_N = spg.get_symmetry_dataset(spg_cell)["number"]
        sg = Spacegroup(sg_N, 1)

        #sites, _ = sg.equivalent_sites(
        #    sites, onduplicates="keep", symprec=0.001
        #)
        muon_pos = []
        for s in sites:
            s_equiv, _ = sg.equivalent_sites(
                [s], onduplicates="keep", symprec=0.001
            )
            muon_pos.append(s_equiv)
    else:
        muon_pos = [sites]
    
    if bool_Cartesian:
        muon_pos = [struct.cell.cartesian_positions(x) for x in muon_pos] # Convert from fractional to absolute coordinates
    #muon_pos = np.array(muon_pos)

    return muon_pos

def calc_single_muon_site_volume(struct, r=0.5, logger=None, bool_symmetrize=True):
    """Calculate volume occupied by a single muon + all its periodic images, assuming they are all more than r away from each other (CAN BE A VERY BAD ASSUMPTION IN SYMMETRIC CRYSTALS)"""
    logger = logging.getLogger(logger)
    
    vol_single_sphere = sphere_volume(r)

    if bool_symmetrize:
        spg_cell = (
            struct.get_cell(),
            struct.get_scaled_positions(),
            struct.get_atomic_numbers(),
        )
        sym = spg.get_symmetry(spg_cell, symprec=0.001)
        num_sym = len(sym['translations'])
#        logger.info(f"Numer of symmetry operations is {num_sym}") #
    else:
        num_sym = 1

    vol_single_muon_site = vol_single_sphere * num_sym

    return vol_single_muon_site, num_sym