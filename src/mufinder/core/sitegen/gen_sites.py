"""Generate candidate muon site positions.

A series of functions to generate candidate muon positions in a
symmetry-reduced subspace of the unit cell.
"""

import logging
import os
import time

import numpy as np
import spglib as spg
from ase import Atoms, io
from ase.build.supercells import make_supercell
from ase.calculators.castep import Castep
from ase.geometry import cell_to_cellpar, get_distances
from ase.spacegroup import Spacegroup, crystal


def gen_sym_equiv(basis, structure, label):
    """Generate symmetry equivalent positions."""
    spg_cell = (
        structure.get_cell(),
        structure.get_scaled_positions(),
        structure.get_atomic_numbers(),
    )

    spacegroup = spg.get_symmetry_dataset(spg_cell)
    replicate = crystal(
        label,
        basis,
        spacegroup=spacegroup["number"],
        cellpar=cell_to_cellpar(structure.get_cell()),
    )
    return replicate


def gen_single_muon(basis, structure, label):
    """Generate structure with single muon in cell."""
    replicate = crystal(
        label, basis, spacegroup=1, cellpar=cell_to_cellpar(structure.get_cell())
    )
    return replicate


def gen_custom(input_struct, site, mode, logger=None):
    """Generate a single muon site from a string."""
    logger = logging.getLogger(logger)
    try:
        new_pos = [float(x) for x in site.split()]
        logger.info(f"Added muon at position with {mode} coordinates {new_pos}.")
        if mode == "absolute":
            new_pos = input_struct.scaled_positions(new_pos)
        return [new_pos]
    except ValueError:
        logging.error("Coordinates entered incorrectly. Please try again.")
        return []


def gen_random(
    input_struct,
    sites=None,
    size=30,
    minr=0.5,
    vdws=1.0,
    attempt_factor=2.0,
    seed=None,
    logger=None,
):
    """Generate random initial muon postions."""
    logger = logging.getLogger(logger)
    rng = np.random.RandomState(seed)

    logger.info(
        (
            "Generating random configurations with the muons being"
            " at least {} angstroms from each other and at least {}"
            " angstroms away from other atoms in the cell..."
        ).format(minr, vdws)
    )

    max_attempts = attempt_factor * size

    if not sites:
        muon_sites = []
    else:
        muon_sites = sites.copy()

    successes = 0
    attempts = 0

    if muon_sites:
        muon_pos = input_struct.cell.cartesian_positions(muon_sites)
    else:
        muon_pos = np.empty((0, 3))

    spg_cell = (
        input_struct.get_cell(),
        input_struct.get_scaled_positions(),
        input_struct.get_atomic_numbers(),
    )

    sg_N = spg.get_symmetry_dataset(spg_cell)["number"]
    sg = Spacegroup(sg_N, 1)

    while attempts < max_attempts:
        cand_frac = rng.rand(3)
        cand_pos = input_struct.cell.cartesian_positions(cand_frac)
        cand_equiv_frac, _ = sg.equivalent_sites(
            cand_frac, onduplicates="warn", symprec=0.001
        )
        cand_equiv_pos = input_struct.cell.cartesian_positions(cand_equiv_frac)

        if muon_pos.size != 0:
            _, cand_muon_dists = get_distances(
                cand_equiv_pos, muon_pos, cell=input_struct.cell, pbc=input_struct.pbc
            )
        else:
            cand_muon_dists = [np.inf]

        _, cand_struct_dists = get_distances(
            cand_pos,
            input_struct.positions,
            cell=input_struct.cell,
            pbc=input_struct.pbc,
        )

        min_muon_dist = np.min(cand_muon_dists)
        min_struct_dist = np.min(cand_struct_dists)

        attempts += 1

        if min_muon_dist >= minr and min_struct_dist >= vdws:
            successes += 1

            muon_sites.append(cand_frac)
            muon_pos = np.append(muon_pos, [cand_pos], axis=0)

        if successes >= size:
            break

    logger.info(f"Generated {successes} additional configurations.")

    if successes > 0:
        return muon_sites[-successes:]
    else:
        return []


def gen_exhaust(
    input_struct,
    sites=None,
    size=30,
    minr=0.5,
    vdws=1.0,
    attempt_factor=2.0,
    seed=None,
    logger=None,
):

    """Generate random initial muon postions until symmetry-reduced subspace is
    completely full."""
    logger = logging.getLogger(logger)

    logger.info(
        (
            "Generating random configurations with the muons being"
            " at least {} angstroms from each other and at least {}"
            " angstroms away from other atoms in the cell..."
        ).format(minr, vdws)
    )

    """Count the number of times gen_random fails to add any additional sites
    in a row"""
    initial_sites = sites.copy()

    failures = 0
    while failures < 10:
        new_sites = gen_random(
            input_struct,
            sites,
            size,
            minr,
            vdws,
            attempt_factor,
        )
        if len(new_sites) > 0:
            sites.extend(new_sites)
            failures = 0
        else:
            failures += 1

    if initial_sites is None:
        successes = len(sites)
    else:
        successes = len(sites) - len(initial_sites)

    logger.info(f"Generated {successes} additional configurations.")

    return 0


def gen_cluster(input_struct, muon_sites, logger=None):
    """Symmetry reduce muon sites in the input structure."""
    logger = logging.getLogger(logger)
    start_time = time.time()

    logger.info(
        "Shifting muon positions to occupy smaller " "subspace of the unit cell..."
    )

    spg_cell = (
        input_struct.get_cell(),
        input_struct.get_scaled_positions(),
        input_struct.get_atomic_numbers(),
    )

    sg_N = spg.get_symmetry_dataset(spg_cell)["number"]
    sg = Spacegroup(sg_N, 1)

    # Calculate all symmetry equivalent muon sites
    sites_equiv, sites_kind = sg.equivalent_sites(
        muon_sites, onduplicates="warn", symprec=0.001
    )

    # Find muon site that is closest to the origin, but still inside cell
    closest_frac = min(
        (m for m in sites_equiv if all(x >= 0 for x in m)), key=np.linalg.norm
    )
    closest_cart = input_struct.cell.cartesian_positions(closest_frac)
    shifted = []

    for i, site in enumerate(muon_sites):
        site_equiv = sites_equiv[np.equal(sites_kind, i)]
        equiv_cart = input_struct.cell.cartesian_positions(site_equiv)

        if equiv_cart.size == 0:
            continue

        _, site_distances = get_distances(
            closest_cart, equiv_cart, cell=input_struct.cell, pbc=input_struct.pbc
        )

        shifted_site = site_equiv[np.argmin(site_distances)]
        shifted.append(shifted_site)

    logger.info("Done.")

    time_taken = time.time() - start_time
    logger.info("Finished in %s seconds.", round(time_taken, 2))

    return shifted


def gen_closeto(input_struct, element, vdws=1.0, seed=None, logger=None):
    """Generate initial muon positions close to atoms of an element."""
    logger = logging.getLogger(logger)
    rng = np.random.RandomState(seed)

    logger.info(
        f"Adding muon positions that are {vdws} angstroms from each of the"
        f" {element} atoms in the unit cell."
    )

    muon_sites = []
    elem_pos = [atom.position for atom in input_struct if atom.symbol == element]

    for atom_pos in elem_pos:
        # Remove atoms that we want to be closer to
        check_struct = input_struct.copy()
        del check_struct[
            [atom.index for atom in input_struct if atom.symbol == element]
        ]

        while True:
            # generate random vector with length equal to vdws
            muon_disp = rng.randn(3)
            muon_disp /= np.linalg.norm(muon_disp, axis=0)
            muon_disp *= vdws

            pos_cart = atom_pos + muon_disp

            if check_struct:
                _, other_dists = get_distances(
                    [pos_cart],
                    check_struct.positions,
                    cell=input_struct.cell,
                    pbc=input_struct.pbc,
                )
            else:
                other_dists = np.array([np.inf])

            # factor 1.6 reflects typical coordination geometries
            if np.min(other_dists) >= 1.6 * vdws:
                pos_frac = input_struct.cell.scaled_positions(pos_cart)
                muon_sites.append(pos_frac)
                break

    logger.info(f"Generated {len(muon_sites)} additional configurations.")

    return muon_sites


def create_input(
    input_struct,
    muon_sites,
    scell_matrix,
    folder,
    rootname,
    musym="H:mu",
    pos_frac=True,
    logger=None,
):
    """Create CASTEP input files for muon positions."""
    logger = logging.getLogger(logger)

    scaled = []

    for pos in muon_sites:
        scaled.append(input_struct.cell @ pos)

    # Muon mass and gyromagnetic ratio
    mass_block = "AMU\n{0}       0.1138000000"
    gamma_block = "radsectesla\n{0}        851586494.1"

    # Check if chosen symbol for muon conflicts with elements in unit cell
    if musym in input_struct.get_chemical_symbols():
        logger.error(
            f'WARNING: Chosen muon symbol "{musym}" already ' f"appears in unit cell."
        )

    # Create a CASTEP calculator
    ccalc = Castep()
    ccalc.cell.species_mass = mass_block.format(musym).split("\n")
    ccalc.cell.species_gamma = gamma_block.format(musym).split("\n")
    ccalc.cell.fix_all_cell = True

    # generate supercell
    sm = np.array([[float(j) for j in i] for i in scell_matrix]).reshape((3, 3))

    scell0 = make_supercell(input_struct, sm)

    if scaled:
        try:
            os.makedirs(folder)
        except FileExistsError:
            logger.error("WARNING! Folder %s already exists." % folder)

        # Create the supercell versions and save them
        for i, mupos in enumerate(scaled):
            # Add the muon to the structure
            scell = scell0.copy() + Atoms("H", positions=[mupos])
            # Add castep custom species
            csp = scell0.get_chemical_symbols() + [musym]

            scell.set_array("castep_custom_species", np.array(csp))
            scell.set_calculator(ccalc)

            outpath = os.path.join(folder, f"{rootname}_{i + 1}.cell")
            io.write(outpath, scell, positions_frac=pos_frac)

        logger.info("Input files written to %s." % folder)

    else:
        logger.info("No muon positions!")


def create_scell(
    input_struct,
    scell_matrix,
    rootname,
    folder,
    pos_frac=True,
    fix_cell=False,
    logger=None,
):
    """Create CASTEP input files for the structure without a muon."""
    logger = logging.getLogger(logger)

    sm = np.array([[float(j) for j in i] for i in scell_matrix]).reshape((3, 3))
    scell = make_supercell(input_struct, sm)

    try:
        os.makedirs(folder)
    except FileExistsError:
        logger.error(f"WARNING! Folder {folder} already exists.")

    ccalc = Castep()
    ccalc.cell.fix_all_cell = bool(fix_cell)
    scell.set_calculator(ccalc)

    fname = os.path.join(folder, "{0}_{1}.cell".format(rootname, 0))
    io.write(fname, scell, positions_frac=pos_frac)

    logger.info(f"Input structure written to CELL file {fname}.")
