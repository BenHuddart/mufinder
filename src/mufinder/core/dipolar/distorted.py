"""Correction for atomic position distortions.

Method of calculating the correction to the dipolar field due to
distortions in the atom positions induced by the muon.
"""

import itertools
import math
import os
import numpy as np
import numpy.linalg as LA

from ase import io, Atoms
from ase.build.supercells import make_supercell
from ase.geometry.geometry import get_distances


from muesr.core import Sample
from muesr.core.atoms import Atoms as muesrAtoms  # ASE and muesr Atoms objects are slightly different
from muesr.engines.clfc import dipten, locfield
from muesr.i_o import load_cif

def ase2muesr(atoms):
    """
    Function to convert ASE atoms object to muesr atoms object
    :param atoms: ASE atoms object
    :return: muesr atoms object
    """
    symbols = atoms.get_chemical_symbols()
    scaled_positions = atoms.get_scaled_positions()
    cell = atoms.get_cell()
    pbc = atoms.get_pbc()

    return muesrAtoms(symbols = symbols, scaled_positions = scaled_positions, cell = cell, pbc = pbc)

def muesr2ase(muesr_atoms):
    """
    Function to convert muesr atoms object to ASE atoms object
    :param muesr_atoms: muesr atoms object
    :return: ASE atoms object
    """
    symbols = muesr_atoms.get_chemical_symbols()
    scaled_positions = muesr_atoms.get_scaled_positions()
    cell = muesr_atoms.get_cell()
    pbc = True

    return Atoms(symbols = symbols, scaled_positions = scaled_positions, cell = cell, pbc = pbc)

def get_supercell_matrix(cell, supercell):
    """
    Function to obtain supercell matrix that was used to generate a supercell
    :param cell: ASE atoms object containing a single unit cell
    :param supercell: ASE atoms object for the supercell
    :return: Supercell matrix
    """
    supercell_mat = supercell.get_cell() @ LA.inv(cell.get_cell())
    return (np.round(supercell_mat)).astype(int)

def generate_fc_supercell(atoms, supercell_mat, fc, k = np.array([0, 0, 0])):
    """
    Generate the Fourier coefficients for the supercell, given those
    for a single unit cell
    :param atoms: ASE atoms object containing a single unit cell
    :param supercell_mat: Supercell matrix
    :param fc: Fourier coefficients for the single unit cell
    :param k: Propagation vector for the magnetic structure
    :return scell: Ase atoms object containing the supercell
    :return fc_supercell: Set of Fourier coefficients for the supercell
    """
    atoms.set_tags(np.array(range(len(atoms))))
    scell = make_supercell(atoms, supercell_mat)
    fc_supercell = []
    for pos, tag in zip(scell.get_scaled_positions(), scell.get_tags()):
        R = supercell_mat @ pos - atoms.get_scaled_positions()[tag]
        phase = 2 * np.pi * np.dot(k, R)
        fc_supercell.append(np.exp(-phase * 1.0j) * fc[tag])

    return scell, np.array(fc_supercell)


def map_structures(structure1, structure2):
    """
    Generate a mapping between equivalent atoms and structure 1 and
    structure 2
    :param structure1: ASE atoms object
    :param structure2: ASE atoms object
    :return: Array where each element corresponds to an atom in structure 1
    with the value of this element telling us which atom in structure 2 this
    is equivalent to
    """
    pos_mat, dist_mat = get_distances(
        structure1.get_positions(),
        structure2.get_positions(),
        cell=structure1.get_cell(),
        pbc=True,
    )

    mapping = np.zeros(len(structure1))
    for i, dist in enumerate(dist_mat):
        j = np.argmin(dist)
        # Check that the atomic species are the same, if not find next
        # smallest distance
        while structure1.get_atomic_numbers()[i] != structure2.get_atomic_numbers()[j]:
            dist[j] = np.inf
            j = np.argmin(dist)

        # Make all members of column j infinite so that atom j can
        # only be mapped to a single atom in structure 1
        dist_mat[:, j] = np.inf
        mapping[i] = j

    return np.array(mapping).astype(int)

def recentre_cell(muon_pos, structure):
    """
    Recentre cell so that the muon site is at [0.5, 0.5, 0.5]
    :param muon_pos: Fractional coordinates of muon site
    :param structure: ASE atoms object
    :return: None
    """
    shifted_pos = []
    for atom_pos in structure.get_scaled_positions(): # this is an ASE atoms object
        shifted_pos.append(atom_pos - muon_pos + [0.5, 0.5, 0.5])

    structure.set_scaled_positions(shifted_pos)

    return 0

def extend_cell(cell, scale_factor, center = np.array([0.5, 0.5, 0.5])):
    """
    Function to extend cell by a given scale factor along each axis
    :param cell: ASE atoms object
    :param scale_factor: Scale factor to multiply each of the unit cell vectors
    :param center: Fractional coordinates in the original cell to be at the center
    of the new cell
    :return extended_cell: ASE atoms object
    """
    extended_cell = cell.copy()
    extended_cell.set_cell(scale_factor * cell.get_cell())
    new_positions = np.array([(pos + center * (scale_factor - 1)) / scale_factor for
                              pos in cell.get_scaled_positions()])
    extended_cell.set_scaled_positions(new_positions)
    return extended_cell


def distortion_correction(mu_struct, indices, cif_file, k, mag_array, select):
    r_corr = []
    D_corr = []
    fc = np.array(mag_array)

    for index in indices:
        dist_struct = mu_struct.valid_struct[index].copy()

        # remove muon
        del dist_struct[[atom.index for atom in dist_struct if atom.symbol=='X']]

        # Need to determine supercell matrix
        single_cell = io.read(cif_file)
        supercell_mat = get_supercell_matrix(single_cell, dist_struct)

        muon_pos = mu_struct.muon_sites_supercell[index]

        k_vec = np.array([float(k[0][0]), float(k[0][1]), float(k[0][2])])
        scell, fc_supercell = generate_fc_supercell(single_cell, supercell_mat, fc, k_vec)

        # For each atom in distorted structure, find the corresponding atom in the undistorted structure
        mapping = map_structures(dist_struct, scell)

        # Recenter both cells on the muon position
        recentre_cell(muon_pos, dist_struct)
        recentre_cell(muon_pos, scell)

        # To protect against atoms moving across the cell boundary we extend the undistorted supercell
        extended_cell = extend_cell(scell, scale_factor=2)

        # Translate atoms in the larger cell by translation vectors such that they are
        # all as close as possible to those in the distorted cell
        new_pos = np.zeros_like(extended_cell.get_scaled_positions())
        for i, dist_pos in enumerate(dist_struct.get_scaled_positions()):

            # find position of equivalent atom in undistorted cell
            j = mapping[i]
            undist_pos = extended_cell.get_scaled_positions()[j]

            # convert coordinates of the atom from the distorted cell into those
            # of the extended cell
            dist_pos_extended = (dist_pos + [0.5, 0.5, 0.5]) / 2

            translated_positions = []
            displacements = []
            for (l, m, n) in itertools.product([-1, 0, 1], repeat=3):
                translated_pos = undist_pos + [0.5 * l, 0.5 * m, 0.5 * n]
                translated_positions.append(translated_pos)
                displacements.append(
                    np.linalg.norm(
                        extended_cell.get_cell()
                        @ (
                            dist_pos_extended - translated_pos
                        )
                    )
                )

            new_pos[j] = translated_positions[np.argmin(displacements)]
        extended_cell.set_scaled_positions(new_pos)

        # Now we are in a position to set up the calculations
        distorted = Sample()
        distorted.cell = ase2muesr(dist_struct)

        # Define magnetic structure
        distorted.new_mm()
        distorted.mm.k = np.array([0, 0, 0])
        distorted.mm.fc = np.array([fc_supercell[m] for m in mapping])

        # Add muon, always at (0.5, 0.5 0.5) since we shifted the cell
        distorted.add_muon([0.5, 0.5, 0.5])

        # Repeat for undistorted cell
        undistorted = Sample()
        undistorted.cell = ase2muesr(extended_cell)

        # Define magnetic structure
        undistorted.new_mm()
        undistorted.mm.k = np.array([0, 0, 0])
        undistorted.mm.fc = fc_supercell
        undistorted.add_muon([0.5, 0.5, 0.5])

        if select == "field":
            # Assume that a radius of 1000 A will include all
            # of the atoms in the cell
            r_dist = locfield(distorted, "s", [1, 1, 1], 1000)
            r_undist = locfield(undistorted, "s", [1, 1, 1], 1000)
            r_corr.append(r_dist[0].D - r_undist[0].D)
        elif select == "tensor":
            # Assume that a radius of 1000 A will include all
            # of the atoms in the cell
            D_dist = dipten(distorted, [1, 1, 1], 1000)
            D_undist = dipten(undistorted, [1, 1, 1], 1000)
            D_corr.append(D_dist[0] - D_undist[0])

    if select == "field":
        return r_corr
    elif select == "tensor":
        return D_corr
