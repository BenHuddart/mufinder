"""Read and write mag_array files in CSV formt."""

import csv
import logging
import os.path

from muesr.core import Sample
from muesr.i_o import load_cif


def decomment(lines, comment_char="#"):
    for line in lines:
        raw = line.split(comment_char)[0].strip()
        if raw:
            yield raw


def write_mag_array(cif_file, num_q, logger=None):
    logger = logging.getLogger(logger)

    xtal = Sample()
    load_cif(xtal, cif_file)

    mag_file = cif_file.replace(".cif", ".mag_array")

    if os.path.exists(mag_file):
        logger.error("Mag_array file already exists.")
        return

    header = ["elem", "a", "b", "c", "mxr", "mxi", "myr", "myi", "mzr", "mzi"]

    with open(mag_file, "w") as f:
        syms = xtal.cell.get_chemical_symbols()
        positions = xtal.cell.get_scaled_positions()
        zeros = [0.0] * (2 * 3)

        wtr = csv.writer(f)
        wtr.writerow(header)

        for i in range(num_q):
            for name, pos in zip(syms, positions):
                wtr.writerow([name, *pos, *zeros])

    logger.info(f"Generated mag_array file {mag_file}.")


def load_mag_array(mag_file, num_q):
    """Parse a mag_array file into a nested list."""
    mag_array = []

    with open(mag_file) as f:
        rdr = csv.DictReader(decomment(f, comment_char="!"))
        for row in rdr:
            if row["elem"] == "elem":
                continue

            mag_array.append(
                [
                    float(row["mxr"]) + float(row["mxi"]) * 1j,
                    float(row["myr"]) + float(row["myi"]) * 1j,
                    float(row["mzr"]) + float(row["mzi"]) * 1j,
                ]
            )

    n_rows = len(mag_array)
    n_atoms = int(n_rows / num_q)

    return [mag_array[start : start + n_atoms] for start in range(0, n_rows, n_atoms)]
