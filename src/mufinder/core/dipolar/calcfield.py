"""Calculate local dipole field at muon sites."""

import logging
import math
import tkinter as tk

import numpy as np
from ase.build.supercells import make_supercell
from muesr.core import Sample
from muesr.engines.clfc import dipten, find_largest_sphere, locfield
from muesr.i_o import load_cif
from muesr.utilities import muon_find_equiv

from mufinder.core.dipolar.distorted import distortion_correction
from mufinder.core.dipolar.ic import ic_calc
from mufinder.core.dipolar.mag_array import load_mag_array


def get_MUC(incomm, structure, num_q, k, mag_array_q):
    """Generate magnetic unit cell."""
    s = []

    if not incomm:
        for i in k[0]:
            if float(i) != 0:
                sx = int(1.0 / float(i))
            else:
                sx = 1
            s.append(sx)
    else:
        # MUC doesn't exist for IC magnetic structures
        s = [3, 3, 3]

    MUC = make_supercell(structure, np.diag(s))
    structure.set_atomic_numbers(
        [x + 1 for x in range(len(structure.get_atomic_numbers()))]
    )
    MUC_copy = make_supercell(structure, np.diag(s))

    full_mag_array = []
    for q in range(num_q):
        single_mag_array = []
        k_vec = np.array([float(k[q][0]), float(k[q][1]), float(k[q][2])])
        mag_array = mag_array_q[q]
        for i, atom_no in enumerate(MUC_copy.get_atomic_numbers()):
            R_vec = (
                MUC.get_scaled_positions()[i] - MUC.get_scaled_positions()[atom_no - 1]
            )
            phase = 2 * math.pi * np.dot(k_vec, R_vec @ np.diag(s))
            single_mag_array.append(
                [np.exp(-phase * 1.0j) * x for x in mag_array[atom_no - 1]]
            )
        if q == 0:
            full_mag_array = single_mag_array

        else:
            full_mag_array = np.add(full_mag_array, single_mag_array)

    # Rescale moments to a reasonable size for visualisation
    if abs(np.max(np.real(full_mag_array))) != 0:
        scale = 5 / abs(np.max(np.real(full_mag_array)))
    else:
        scale = 5
    MUC.set_momenta(scale * np.real(full_mag_array))
    return MUC


def enter_pos(logger=None):
    logger = logging.getLogger(logger)

    muon_pos = []
    indices = []
    num = 1

    while True:
        pos = tk.simpledialog.askstring(
            "Input",
            "Enter fractional coordinates for muon position %i."
            ' Type "done" if finished.\n' % num,
        )

        if pos == "done":
            break
        else:
            try:
                muon_pos.append([float(x) for x in pos.split()])
                indices.append(num)

                num += 1
            except ValueError:
                logger.error("Coordinates entered incorrectly. Please try again.")

    return muon_pos, indices


def calc_field(
    mu_struct,
    options,
    indices,
    mupos,
    cif_file,
    num_q,
    k,
    dipole_fields,
    n,
    mag_file,
    field_dist,
    shape_factor,
    logger=None,
):
    logger = logging.getLogger(logger)

    # Destructuring options
    incommensurate, distorted, mode, find_equiv, vector, lorentz = options

    # Define crystal structure
    xtal = Sample()
    load_cif(xtal, cif_file)

    # Define magnetic structure
    xtal.new_mm()
    xtal.mm.k = np.array([k[0][0], k[0][1], k[0][2]])

    # Use the mag_array for first k
    mag_array = load_mag_array(mag_file, num_q)[0]
    xtal.mm.fc = np.array(mag_array)

    # Add muons
    if mode == "manual":
        for pos in mupos:
            xtal.add_muon(pos)
    elif mode == "auto":
        for index in indices:
            muon_pos = mu_struct.muon_sites[index]
            xtal.add_muon([muon_pos[0], muon_pos[1], muon_pos[2]])
    else:
        logger.error(f"Unknown calc_field mode {mode}")

    if find_equiv:
        old_len = len(xtal.muons)
        muon_find_equiv(xtal)
        new_len = len(xtal.muons)
        new_index = []
        for i in range(old_len):
            for j in range(int(new_len / old_len)):
                new_index.append(indices[i])
        indices = new_index

    radius = find_largest_sphere(xtal, [n, n, n])

    if incommensurate:
        ic_calc(
            xtal,
            n,
            radius,
            cif_file,
            field_dist,
            lorentz,
            shape_factor,
            logger=f"{logger.name}.ic_calc",
        )

    else:
        r = locfield(xtal, "s", [n, n, n], radius)

        no_muons = len(r)

        if distorted:
            # Include corrections to dipolar field due to distortions
            # of nearby magnetic atoms
            r_corr = distortion_correction(
                mu_struct, indices, cif_file, k, mag_array, "field"
            )

        else:
            r_corr = []
            for i in range(0, no_muons):
                r_corr.append([0, 0, 0])

        r_tot = []
        for i in range(0, no_muons):
            if lorentz:
                r_tot.append(r[i].D + r_corr[i] + (1 - 3 * shape_factor) * r[i].L)
            else:
                r_tot.append(r[i].D + r_corr[i])

        if vector:
            B_avg = np.array([0.0, 0.0, 0.0])

            for i, r_i in zip(indices, r_tot):
                logger.info(f"Muon site {i} has Bdip = {r_i} T")
                B_avg += r_i
        else:
            B_avg = 0
            # Print fields to screen
            for i, r_i in zip(indices, r_tot):
                r_norm = np.linalg.norm(r_i, axis=0)
                logger.info(f"Muon site {i} has Bdip = {r_norm:.6f} T")
                B_avg += r_norm

        if no_muons > 1:
            B_avg = B_avg / no_muons
            logger.info(f"Average dipolar field is {B_avg:.6f} T")

        # Store dipolar fields
        dipole_fields[0] = r


def calc_tensor(
    mu_struct, options, indices, mupos, cif_file, num_q, k, n, mag_file, logger=None
):
    """Calculate the dipolar tensor (for commensurate structures)."""
    logger = logging.getLogger(logger)

    # Destructuring options
    incommensurate, distorted, mode, find_equiv, vector, lorentz = options

    # Define crystal structure
    xtal = Sample()
    load_cif(xtal, cif_file)

    # Define magnetic structure
    xtal.new_mm()
    xtal.mm.k = np.array([k[0][0], k[0][1], k[0][2]])

    # Just use mag_array for first k
    mag_array = load_mag_array(mag_file, num_q)[0]
    xtal.mm.fc = np.array(mag_array)

    # Add muons
    if mode == "manual":
        for pos in mupos:
            xtal.add_muon(pos)

    if mode == "auto":
        for index in indices:
            muon_pos = mu_struct.muon_sites[index]
            xtal.add_muon([muon_pos[0], muon_pos[1], muon_pos[2]])

    if find_equiv:
        old_len = len(xtal.muons)
        muon_find_equiv(xtal)
        new_len = len(xtal.muons)
        new_index = []
        for i in range(old_len):
            for j in range(int(new_len / old_len)):
                new_index.append(indices[i])
        indices = new_index

    radius = find_largest_sphere(xtal, [n, n, n])

    D = dipten(xtal, [n, n, n], radius)

    no_muons = len(D)

    if distorted:
        # Include corrections to dipolar field due to distortions of
        # nearby magnetic atoms
        D_corr = distortion_correction(
            mu_struct, indices, cif_file, k, mag_array, "tensor"
        )

    else:
        D_corr = []
        for i in range(0, no_muons):
            D_corr.append(np.zeros((3, 3)))

    D_tot = []
    for i in range(0, no_muons):
        D_tot.append(D[i] + D_corr[i])

    D_avg = np.zeros((3, 3))

    # Print fields to screen
    for i in range(0, no_muons):
        logger.info(f"Muon site {indices[i]} has D_dip = {D_tot[i]} T / μ_B")
        D_avg += D_tot[i]

    if no_muons > 1:
        D_avg = D_avg / no_muons
        logger.info(f"Average dipolar tensor is {D_avg} T / μ_B")
