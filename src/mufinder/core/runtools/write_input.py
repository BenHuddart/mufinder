import logging
import re
import shutil
from pathlib import Path

from ase import io


def find_cell_files(cell_dir, site_filter=None):
    """Find cell files in directory and sort by site index."""
    cell_files = []

    for p in cell_dir.glob("*.cell"):
        site = int(re.split("[_ .]", p.name)[-2])

        if site_filter is not None and site not in site_filter:
            continue

        cell_files.append((site, p))

    cell_files.sort(key=lambda p: p[0])

    return cell_files


def create_site_tree(
    cell_files,
    out_dir,
    castep_params=None,
    custom_cell=None,
    custom_params=None,
):
    if castep_params is None:
        castep_params = {}

    out_dir = Path(out_dir)
    out_dir.mkdir(parents=True, exist_ok=True)

    seed_paths = []

    for site, input_cell in cell_files:
        folder = out_dir / f"site{site}"

        if folder.is_dir():
            shutil.rmtree(folder)

        folder.mkdir(parents=True, exist_ok=False)

        cell_path = folder / input_cell.name

        shutil.copyfile(input_cell, cell_path)
        write_to_cell(custom_cell, cell_path, castep_params["kpoints"])

        param_path = cell_path.with_suffix(".param")
        seed_path = cell_path.with_suffix("")

        if custom_params is None:
            charge_j = None if site == 0 else castep_params["charge"]
            param_text = get_param_text(**dict({i:castep_params[i] for
                        i in castep_params if i!='kpoints'}, charge=charge_j))
        else:
            param_text = custom_params

        param_path.write_text(param_text)
        seed_paths.append((site, seed_path))

    return seed_paths


def write_kpoints(mpgrid):
    """Create CASTEP kpoint grid text."""
    try:
        mpgrid = [int(k) for k in mpgrid[0]]
    except:
        mpgrid = [0, 0, 0]
    if all(k > 0 for k in mpgrid):
        return "\nkpoint_mp_grid %i %i %i" % (mpgrid[0], mpgrid[1], mpgrid[2])
    else:
        return ""


def write_to_cell(cell_text, cell_file, mpgrid):
    """Write CASTEP cell file."""
    cell_text = write_kpoints(mpgrid) + "\n" + (cell_text or "")
    with open(cell_file, "a") as f:
        f.write(cell_text)


def get_param_text(*, charge, xc, spin_pol, basis_prec, solver):
    """Generate CASTEP param text for muon site geometry optimisation."""
    param_text = "task: geometryoptimisation\n"

    if charge == "mu+":
        param_text += "charge: +1\n"

    param_text += "xc_functional: %s\n" % xc

    if spin_pol:
        param_text += "spin_polarized : true\n"
    else:
        param_text += "spin_polarized : false\n"

    param_text += "basis_precision : %s\n" % basis_prec
    param_text += "write_cell_structure : true\n"
    param_text += "geom_max_iter : 300\n"
    param_text += "max_scf_cycles : 100\n"

    if solver == "edft":
        param_text += "elec_method : edft\n"

    param_text += "continuation : default"

    return param_text


def cell2cif(cell_file, logger=None):
    """Convert a CASTEP cell file to CIF format."""
    logger = logging.getLogger(logger)

    structure = io.read(cell_file)
    cifname = cell_file.replace("_0-out.cell", "_relaxed.cif")
    io.write(cifname, structure)

    logger.info(f"Relaxed structure can be found in {cifname}.")

    return cifname
