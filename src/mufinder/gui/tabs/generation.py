import logging
import os
import threading
import time
import tkinter as tk
import tkinter.filedialog as tkfd
import tkinter.scrolledtext as tkst
import tkinter.ttk as ttk
from pathlib import Path

import numpy as np
from ase import io
from ase.atoms import Atoms
from ase.build.supercells import make_supercell

from mufinder.core.sitegen.calc_volume import (
    calc_free_volume,
    calc_single_muon_site_volume,
    get_all_muon_sites,
)
from mufinder.core.sitegen.gen_sites import (
    create_input,
    create_scell,
    gen_closeto,
    gen_cluster,
    gen_custom,
    gen_exhaust,
)
from mufinder.gui.config import gen
from mufinder.gui.custom_view import custom_view
from mufinder.gui.widgets.get_numbers import get_numbers
from mufinder.gui.widgets.table import SimpleTableInput
from mufinder.gui.widgets.text_handler import TextHandler
from mufinder.gui.widgets.textfield import TextField

# from ase.geometry import get_duplicate_atoms


# TODO: Handle case of site generation without structure


class GenerationTab(ttk.Notebook):
    def __init__(self, *args, **kwargs):
        # super().__init__(*args, **kwargs)
        ttk.Frame.__init__(self, *args, **kwargs)

        tk.Label(self, text="Input structure:").grid(row=0, column=0, sticky=tk.W)
        self.input = tk.Entry(self)
        self.input.grid(row=1, sticky=tk.W)

        ttk.Button(self, text="Load", width=10, command=self.set_struct).grid(
            row=2, column=0, sticky=tk.W
        )

        self.inputf = TextField(
            self,
            "folder name",
            3,
            0,
            "input",
            "folder to contain input files for calculations",
        )

        self.rootname = TextField(
            self, "rootname", 5, 0, "", "Root name for input files"
        )

        tk.Label(self, text="Coordinates system:").grid(row=7, column=0, sticky=tk.W)

        mode_list = ["fractional", "absolute"]
        self.mode = tk.StringVar(self)
        self.mode.set(mode_list[0])
        mode_choice = ttk.OptionMenu(self, self.mode, mode_list[0], *mode_list)
        mode_choice.grid(row=8, column=0, sticky=tk.W)

        atom_list = [""]
        self.om_variable = tk.StringVar(self)
        self.om_variable.set(atom_list[0])

        self.atom_choice = ttk.OptionMenu(
            self, self.om_variable, atom_list[0], *atom_list
        )
        self.atom_choice.grid(row=10, column=0)

        self.musym = TextField(
            self,
            "Symbol for muon",
            0,
            1,
            "H:mu",
            "symbol used to represent muon in calculations",
        )

        self.minr = TextField(
            self,
            "muon-muon distance",
            2,
            1,
            "0.5",
            "Minimum distance between initial muon positions",
        )
        self.minr_num = TextField(
            self,
            "Target # of muon sites",
            2,
            1,
            "30",
            "Expected number of initial muon positions",
            tk.E,
        )
        ttk.Button(self, text="→", command=self.estimate_minr_num).grid(
            row=2, column=1, sticky=""
        )
        ttk.Button(self, text="←", command=self.estimate_minr).grid(
            row=3, column=1, sticky=""
        )

        self.vdws = TextField(
            self,
            "muon-atom distance",
            4,
            1,
            "1.0",
            "Minimum distance between initial muon position and atoms in the structure",
        )

        self.tol = TextField(
            self,
            "Tolerance for clustering",
            6,
            1,
            "1.0",
            (
                "Maximum distance between muon positions for them to "
                'be considered "connected"'
            ),
        )

        variable = tk.StringVar(self)
        variable.set("none")  # default value
        tk.Label(self, text="Supercell matrix").grid(row=8, column=1, sticky=tk.W)

        self.scell_matrix = SimpleTableInput(self, 3, 3)
        self.scell_matrix.grid(row=9, column=1, rowspan=2, sticky=tk.W)

        self.num = tk.Entry(self)
        self.num.grid(row=11, column=1, sticky=tk.E)

        cancel = ttk.Button(
            self, text="Delete muon positions", command=self.delete_sites
        )
        cancel.grid(row=11, column=1, sticky=tk.W, pady=4)

        self.output_text = tkst.ScrolledText(self, height=10, width=100)
        self.output_text.grid(column=0, row=14, columnspan=2)

        self.logger = logging.getLogger("generation")
        self.logger.addHandler(TextHandler(self.output_text))
        self.logger.setLevel(logging.INFO)

        ttk.Button(self, text="Add single muon", command=self.run_custom).grid(
            row=11, column=0, sticky=tk.W, pady=4
        )
        ttk.Button(self, text="Attach muon to element", command=self.run_closeto).grid(
            row=10, column=0, sticky=tk.W, pady=4
        )
        ttk.Button(self, text="Fill cell with muons", command=self.run_random).grid(
            row=9, column=0, sticky=tk.W, pady=4
        )

        self.sym = tk.IntVar(value=1)
        ttk.Checkbutton(
            self,
            text="Symmetry-reduced positions",
            variable=self.sym,
        ).grid(row=9, column=0, sticky="", pady=4)

        self.count = TextField(
            self,
            "Number of muon sites",
            15,
            0,
            "0",
            ("Number of initial muons positions that have been generated"),
        )

        # ttk.Button(self, text="Volume estimate", command=self.estimate_minr_num).grid(
        #    row=17, column=0, sticky=tk.W, pady=4
        # )

        b_view = ttk.Button(
            self, text="View initial muon positions", command=self.view_struct
        )
        b_view.grid(row=17, column=0, sticky=tk.W, pady=4)

        b_view = ttk.Button(
            self, text="Create muonated input files", command=self.write_files
        )
        b_view.grid(row=16, column=1, sticky=tk.W, pady=4)

        b_view = ttk.Button(
            self, text="Create pristine input file", command=self.cell_only
        )
        b_view.grid(row=15, column=1, sticky=tk.W, pady=4)

        self.fix_cell = tk.IntVar(value=0)
        ttk.Checkbutton(self, text="Fix cell", variable=self.fix_cell).grid(
            row=15, column=1, sticky=tk.E, pady=4
        )

        ttk.Button(
            self, text="List initial muon positions", command=self.list_sites
        ).grid(row=18, column=0, sticky=tk.W, pady=4)

        ttk.Button(self, text="Quit", command=self.quit).grid(
            row=19, column=0, sticky=tk.W, pady=4
        )

    def set_struct(self):
        try:
            input_struct = tkfd.askopenfilename()

            if input_struct != "":
                self.input.delete(0, tk.END)
                self.input.insert(tk.END, os.path.normpath(input_struct))

                struct = io.read(input_struct)
                gen.input_struct = struct
                atom_list = list(set(struct.get_chemical_symbols()))

                menu = self.atom_choice["menu"]
                menu.delete(0, "end")
                for string in atom_list:
                    menu.add_command(
                        label=string,
                        command=lambda value=string: self.om_variable.set(value),
                    )

                if ".cif" in str.lower(input_struct):
                    file_type = "CIF"
                elif "cell" in str.lower(input_struct):
                    file_type = "CELL"
                self.logger.info(f"{input_struct} loaded as input {file_type} file.")

                input_root = (
                    os.path.basename(self.input.get())
                    .replace(".cif", "")
                    .replace(".cell", "")
                )
                bad_chars = "() "
                for c in bad_chars:
                    input_root = input_root.replace(c, "")

                self.rootname.replace(input_root)

                try:
                    custom_view(make_supercell(gen.input_struct, np.identity(3)))
                except UnicodeDecodeError:
                    pass

        except Exception as e:
            self.logger.error("Error loading file. Please try again.", e)

    def run_custom(self):
        gen.mode = self.mode.get()
        site = tk.simpledialog.askstring(
            "Input", f"Enter {gen.mode} coordinates for initial muon position.\n"
        )

        gen.site_list.extend(
            gen_custom(
                gen.input_struct,
                site,
                gen.mode,
                logger=f"{self.logger.name}.gen_custom",
            )
        )

        if self.sym:
            gen.site_list = gen_cluster(
                gen.input_struct,
                gen.site_list,
                logger=f"{self.logger.name}.gen_cluster",
            )

    def run_closeto(self):
        if self.om_variable.get() == "":
            self.logger.info("Please select an element.")
            return

        def inner():
            elem = self.om_variable.get()
            gen.vdws = float(self.vdws.get())
            gen.site_list = gen_closeto(
                gen.input_struct,
                elem,
                gen.vdws,
                logger=f"{self.logger.name}.gen_closeto",
            )

            if self.sym:
                gen.site_list = gen_cluster(
                    gen.input_struct,
                    gen.site_list,
                    logger=f"{self.logger.name}.gen_cluster",
                )

        thread = threading.Thread(target=inner)
        thread.start()

    def symm_shift(self):
        def inner():
            gen.site_list = gen_cluster(
                gen.input_struct,
                gen.site_list,
                logger=f"{self.logger.name}.gen_cluster",
            )

        thread = threading.Thread(target=inner)
        thread.start()

    def run_random(self):
        def inner():
            gen.minr = float(self.minr.get())
            gen.vdws = float(self.vdws.get())

            gen_exhaust(
                gen.input_struct,
                sites=gen.site_list,
                minr=gen.minr,
                vdws=gen.vdws,
                logger=f"{self.logger.name}.gen_exhaust",
            )

            if self.sym:
                gen.site_list = gen_cluster(
                    gen.input_struct,
                    gen.site_list,
                    logger=f"{self.logger.name}.gen_cluster",
                )

        thread = threading.Thread(target=inner)
        thread.start()

    def view_struct(self):
        """View all muon positions within the supercell."""
        if len(gen.site_list) > 0:
            site_list_cart = list(
                gen.input_struct.cell.cartesian_positions(gen.site_list)
            )

            # Create the supercell ase.Atoms object
            sm = np.array(
                [[float(j) for j in i] for i in self.scell_matrix.get()]
            ).reshape((3, 3))
            all_muons = make_supercell(gen.input_struct, sm).copy()

            # Add the muon to the structure
            all_muons += Atoms(
                "X{}".format(len(site_list_cart)), positions=site_list_cart
            )
            custom_view(all_muons)
        else:
            self.logger.info(
                "No muon positions generated, showing pristine structure instead."
            )
            custom_view(make_supercell(gen.input_struct, np.identity(3)))

    def list_sites(self):
        gen.mode = self.mode.get()

        if gen.mode == "absolute":
            sites = list(gen.input_struct.cell.cartesian_positions(gen.site_list))
        else:
            sites = gen.site_list

        for i, site in enumerate(sites, 1):
            self.logger.info(f"Initial position {i} has {gen.mode} coordinates {site}.")

    def delete_sites(self):
        if self.num.get() != "":
            site_ids = get_numbers(
                self.num.get(), logger=f"{self.logger.name}.delete_sites"
            )
        else:
            site_ids = [i for i in range(1, len(gen.site_list) + 1)]

        num_removed = 0
        for site_id in site_ids:
            gen.site_list.pop(site_id - num_removed - 1)
            self.logger.info(f"Removed initial muon position {site_id}.")
            num_removed += 1

    def write_files(self):
        gen.symbol = self.musym.get()
        gen.scell_mat = self.scell_matrix.get()
        gen.mode = self.mode.get()
        inputf_full = Path(self.input.get()).parent / self.inputf.get()
        create_input(
            gen.input_struct,
            gen.site_list,
            gen.scell_mat,
            inputf_full,
            self.rootname.get(),
            musym=gen.symbol,
            pos_frac=(gen.mode == "fractional"),
            logger=f"{self.logger.name}.create_input",
        )

    def cell_only(self):
        """Generate input cell containing structure only (without a muon)."""
        inputf_full = Path(self.input.get()).parent / self.inputf.get()
        gen.scell_mat = self.scell_matrix.get()

        create_scell(
            gen.input_struct,
            gen.scell_mat,
            self.rootname.get(),
            inputf_full,
            pos_frac=(gen.mode == "fractional"),
            fix_cell=self.fix_cell.get(),
            logger=f"{self.logger.name}.create_scell",
        )

    def estimate_minr_num(self):
        """Estimates the expected number of muon sites and saves the
        result to the minr_num text field.

        Also prints the volumes of the unit cell, free space around
        atoms in principle available for muon sites, and the volume of
        the actually generated muon sites

        """

        if gen.input_struct == []:
            self.logger.info("No structure loaded.")
            return

        def inner():
            tic = time.perf_counter()  #

            muon_r = float(self.minr.get()) / 2  # gen.minr/2
            atom_r = float(self.vdws.get()) - muon_r  # gen.vdws - muon_r

            vol_cell = gen.input_struct.get_volume()

            vol_free = calc_free_volume(
                gen.input_struct,
                r=atom_r,
                # float(self.vdws.get()) - float(self.minr.get()),
                # gen.vdws - gen.minr,
                logger=f"{self.logger.name}.calc_free_volume",
            )

            muon_sites_pos = get_all_muon_sites(
                gen.input_struct,
                sites=gen.site_list,
                bool_symmetrize=True,
                bool_Cartesian=True,
            )
            # self.logger.info(f"{muon_sites_pos}")
            vol_muons = 0
            for mp in muon_sites_pos:
                vol_muons = (
                    vol_muons
                    + vol_cell
                    - calc_free_volume(
                        gen.input_struct,
                        r=muon_r,  # float(self.minr.get()), #gen.minr,
                        logger=f"{self.logger.name}.calc_free_volume",
                        atom_pos=mp,
                        verbose=False,  #
                    )
                )

            vol_single_muon_site, num_sym = calc_single_muon_site_volume(
                gen.input_struct,
                r=muon_r,  # gen.minr/2,
                logger=f"{self.logger.name}.calc_single_muon_site_volume",
            )

            # Estimated by Monte Carlo to within 0.001 to be optimal
            # for the current muon-site generation algorithm!
            expected_random_filling_fraction = 0.350

            toc = time.perf_counter()  #

            expected_num_sites = np.floor(
                expected_random_filling_fraction * vol_free / vol_single_muon_site
            ).astype(int)

            pct_free = 100 * (vol_free / vol_cell)
            pct_muons = 100 * (vol_muons / vol_cell)

            self.logger.info(
                f"Cell volume {vol_cell:0.2f} A^3, "
                f"available to muons {vol_free:0.2f} A^3 ({pct_free:0.1f}%), "
                f"occupied by muons {vol_muons:0.2f} A^3 ({pct_muons:0.1f}%)."
            )

            muon_num_best = np.floor(vol_free / vol_single_muon_site).astype(int)
            expected_pct = (
                100 * expected_random_filling_fraction * (vol_free / vol_cell)
            )

            self.logger.info(
                f" IF NO OVERLAP: "
                f"each muon site is {vol_single_muon_site:0.2f} A^3, "
                f"best is {muon_num_best} sites, "
                f"expected is {expected_num_sites} "
                f"({expected_pct:0.1f}%) sites."
            )

            self.logger.info(f"Finished in {(toc - tic):0.4f} seconds.")

            self.minr_num.replace(expected_num_sites)

        thread = threading.Thread(target=inner)
        thread.start()

    def estimate_minr(self):
        """Estimate the expected muon-muon distance.

        Uses the expected number of muon sites and saves the result to
        the minr text field.

        """

        if gen.input_struct == []:
            self.logger.info("No structure loaded.")
            return

        def inner():
            expected_num_sites = float(self.minr_num.get())
            muon_r = float(self.minr.get()) / 2  # gen.minr/2
            atom_r = float(self.vdws.get()) - muon_r  # gen.vdws - muon_r

            vol_free = calc_free_volume(
                gen.input_struct,
                r=atom_r,
                # float(self.vdws.get()) - float(self.minr.get()),
                # gen.vdws - gen.minr,
                logger=f"{self.logger.name}.calc_free_volume",
            )

            vol_single_muon_site, num_sym = calc_single_muon_site_volume(
                gen.input_struct,
                r=muon_r,
                logger=f"{self.logger.name}.calc_single_muon_site_volume",
            )

            # Estimated by Monte Carlo to within 0.001 to be optimal
            # for the current muon-site generation algorithm!

            expected_random_filling_fraction = 0.350

            muon_r = muon_r * np.cbrt(
                expected_random_filling_fraction
                * vol_free
                / (vol_single_muon_site * expected_num_sites)
            )

            self.minr.replace(2 * muon_r)

        thread = threading.Thread(target=inner)
        thread.start()
