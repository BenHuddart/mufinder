import logging
import os
import pickle
import threading
import tkinter as tk
import tkinter.scrolledtext as tkst
import tkinter.ttk as ttk

import numpy as np
from ase import io
from ase.build.supercells import make_supercell

from mufinder.core.clustering.clustering import cluster, single_cluster
from mufinder.core.clustering.displacements import plot_radial_displacement
from mufinder.gui.config import aout
from mufinder.gui.custom_view import custom_view
from mufinder.gui.widgets.text_handler import TextHandler
from mufinder.gui.widgets.textfield import TextField
from mufinder.gui.widgets.tooltip import ToolTip


class AnalysisTab(ttk.Notebook):
    def __init__(self, *args, **kwargs):
        # super().__init__(*args, **kwargs)
        ttk.Frame.__init__(self, *args, **kwargs)

        tk.Label(self, text="Input structure:").grid(row=0, column=0, sticky=tk.W)
        self.input = tk.Entry(self)
        self.input.grid(row=1, sticky=tk.W)
        ttk.Button(self, text="Load", width=10, command=self.set_struct).grid(
            row=2, column=0, sticky=tk.W
        )

        putput_dir_lbl = tk.Label(self, text="Results directory:")
        putput_dir_lbl.grid(row=3, column=0, sticky=tk.W)
        self.output_dir = tk.Entry(self)
        self.output_dir.grid(row=4, sticky=tk.W)
        ToolTip(putput_dir_lbl, "folder containing output cell and .castep files")

        self.shift = tk.IntVar()
        tk.Checkbutton(
            self,
            text="Shift sites to symmetry equivalent positions",
            variable=self.shift,
        ).grid(row=7, column=0, sticky=tk.W)
        ttk.Button(self, text="Load", width=10, command=self.load_dir).grid(
            row=5, column=0, sticky=tk.W
        )

        self.musym = TextField(
            self,
            "Symbol for muon",
            0,
            1,
            "",
            "symbol used to represent muon in calculations",
        )

        self.algorithm = tk.StringVar(self)
        algorithm_options = ["connected components", "k-means"]

        tk.Label(self, text="Clustering algorithm").grid(row=2, column=1, sticky=tk.W)
        ttk.OptionMenu(
            self, self.algorithm, algorithm_options[0], *algorithm_options
        ).grid(row=3, column=1, sticky=tk.W)

        self.param = TextField(
            self,
            "Tolerance for clustering",
            4,
            1,
            "1.0",
            "Maximum distance between muon positions"
            ' for them to be considered "connected"',
        )

        self.use_disp = tk.IntVar()
        use_disp_tick = tk.Checkbutton(
            self,
            text="Maximum allowed displacement",
            variable=self.use_disp,
            command=self.activateCheck,
        )
        use_disp_tick.grid(row=6, column=1, sticky=tk.W)
        self.max_disp = tk.Entry(self)
        self.max_disp.grid(row=7, column=1, sticky=tk.W)
        self.max_disp.config(state=tk.DISABLED)

        ToolTip(
            use_disp_tick,
            "Discard structures including ionic displacements"
            " that exceed the specified value",
        )

        self.output_text = tkst.ScrolledText(self, height=10, width=100)
        self.output_text.grid(column=0, row=9, columnspan=2)

        self.logger = logging.getLogger("analysis")
        self.logger.addHandler(TextHandler(self.output_text))
        self.logger.setLevel(logging.INFO)

        ttk.Button(self, text="Run", command=self.run_clustering).grid(
            row=8, column=0, sticky=tk.W, pady=4
        )
        ttk.Button(self, text="View muon sites", command=self.view_struct2).grid(
            row=14, column=0, sticky=tk.W, pady=4
        )

        self.cluster_no = tk.StringVar(self)
        self.cluster_list = ["All"]
        self.which_cluster = ttk.OptionMenu(
            self, self.cluster_no, self.cluster_list[0], *self.cluster_list
        )
        self.which_cluster.grid(row=14, column=0, sticky=tk.E)

        ttk.Button(self, text="List muon sites", command=self.list_sites).grid(
            row=15, column=0, sticky=tk.E, pady=4
        )

        write_cif = ttk.Button(self, text="Write cif file", command=self.write_to_cif)
        write_cif.grid(row=14, column=1, sticky=tk.E, pady=4)
        ToolTip(write_cif, "write cif file containing muons embedded within structure")

        save_sites = ttk.Button(self, text="Save sites", command=self.save_structures)
        save_sites.grid(row=15, column=0, sticky=tk.W, pady=4)
        ToolTip(
            save_sites,
            "Write a .pkl file containing the results" " of the clustering analysis",
        )

        load_sites = ttk.Button(self, text="Load sites", command=self.load_structures)
        load_sites.grid(row=15, column=1, sticky=tk.E, pady=4)
        ToolTip(load_sites, "Load results of previous analysis from a .pkl file")

        self.grid_rowconfigure(16, weight=1)
        single_site_frame = tk.LabelFrame(self, text="Single site analysis", pady=2)
        single_site_frame.grid(
            row=16, columnspan=2, sticky="WE", padx=2, pady=2, ipadx=2, ipady=2
        )

        # use empty column to space out widgets
        single_site_frame.grid_columnconfigure(2, weight=1)

        singles_label = tk.Label(single_site_frame, text="Site index")
        singles_label.grid(row=17, column=0, sticky=tk.W)
        self.singles = tk.Entry(single_site_frame)
        self.singles.grid(row=18, column=0, sticky=tk.W)
        ToolTip(singles_label, "Index of muon stopping site to analyse")

        ttk.Button(single_site_frame, text="Update", command=self.view_struct3).grid(
            row=19, column=0, sticky=tk.W, pady=4
        )

        ttk.Button(
            single_site_frame, text="View displacements", command=self.view_disp
        ).grid(row=20, column=0, sticky=tk.W, pady=4)

        energy_label = tk.Label(single_site_frame, text="Energy (eV)")
        energy_label.grid(row=17, column=3, sticky=tk.E)
        self.energy = tk.Entry(single_site_frame)
        self.energy.grid(row=18, column=3, sticky=tk.E)
        ToolTip(
            energy_label,
            "Energy of muon stopping site relative to energy of lowest energy site.",
        )

        position_label = tk.Label(single_site_frame, text="Muon position")
        position_label.grid(row=17, column=2, sticky=tk.W)

        self.muon_position_a = tk.Entry(single_site_frame)
        self.muon_position_b = tk.Entry(single_site_frame)
        self.muon_position_c = tk.Entry(single_site_frame)

        self.muon_position_a.grid(row=18, column=2, sticky=tk.W)
        self.muon_position_b.grid(row=19, column=2, sticky=tk.W)
        self.muon_position_c.grid(row=20, column=2, sticky=tk.W)

        ToolTip(
            position_label, "Fractional coordinates of muon site in conventional cell"
        )

        write_cif = ttk.Button(
            single_site_frame,
            text="Write structure to cif",
            command=self.write_single_cif,
        )
        write_cif.grid(row=19, column=3, sticky=tk.E, pady=4)
        ToolTip(write_cif, "Write cif file containing muons embedded within structure")

        ttk.Button(self, text="Quit", command=self.quit).grid(
            row=20, column=0, sticky=tk.W, pady=4
        )

        self.prev_algorithm = "connected components"

        self.update_cluster_list()
        self.algorithm_update()


    def algorithm_update(self):
        if self.algorithm.get() != self.prev_algorithm:
            if self.algorithm.get() == "connected components":
                self.param.change_label(
                    "Tolerance for clustering",
                    "Maximum distance between muon positions"
                    ' for them to be considered "connected"',
                )
            if self.algorithm.get() == "k-means":
                self.param.change_label(
                    "Number of clusters",
                    "Expected number of clusters for k-means clustering",
                )
            self.prev_algorithm = self.algorithm.get()
        self.after(1000, func=self.algorithm_update)

    def update_cluster_list(self):
        if aout.clust_composition:
            new_cluster_list = ["All"] + [str(i+1) for i in range(len(aout.clust_composition))]
            if new_cluster_list != self.cluster_list:
                self.cluster_list = new_cluster_list
                menu = self.which_cluster["menu"]
                menu.delete(0, "end")
                for string in self.cluster_list:
                    menu.add_command(
                        label=string,
                        command=lambda value=string: self.cluster_no.set(value),
                    )

                self.cluster_no.set(self.cluster_list[0])

        self.after(1000, func=self.update_cluster_list)

    def set_struct(self):
        try:
            input_struct = tk.filedialog.askopenfilename()

            if input_struct != "":

                struct = io.read(input_struct)

                self.input.delete(0, tk.END)
                self.input.insert(tk.END, os.path.normpath(input_struct))

                if ".cif" in str.lower(input_struct):
                    file_type = "CIF"
                elif "cell" in str.lower(input_struct):
                    file_type = "CELL"

                self.logger.info(f"{input_struct} loaded as input {file_type} file.")

                try:
                    custom_view(make_supercell(struct, np.identity(3)))
                except UnicodeDecodeError:
                    pass
        except Exception:
            self.logger.info("Error loading file.  Please try again.")

    def view_struct2(self):
        if aout.shifted == [None]:
            self.logger.info("Clustering not yet run.")
        elif self.cluster_no.get() == 'All':
            custom_view(aout.shifted)
        else:
            custom_view(single_cluster(aout, self.cluster_no.get()))

    def view_struct3(self):
        if aout.valid_struct == [None]:
            self.logger.info("Clustering not yet run.")
        else:
            self.energy.delete(0, tk.END)
            self.muon_position_a.delete(0, tk.END)
            self.muon_position_b.delete(0, tk.END)
            self.muon_position_c.delete(0, tk.END)

            single_struct = aout.valid_struct
            singles_int = int(self.singles.get())

            rel_energy = (
                single_struct[singles_int].get_potential_energy()
                - single_struct[0].get_potential_energy()
            )

            self.energy.insert(tk.END, rel_energy)
            self.muon_position_a.insert(tk.END, aout.muon_sites[singles_int][0])
            self.muon_position_b.insert(tk.END, aout.muon_sites[singles_int][1])
            self.muon_position_c.insert(tk.END, aout.muon_sites[singles_int][2])

            custom_view(single_struct[singles_int])

    def view_disp(self):
        if aout.valid_struct == [None]:
            self.logger.info("Clustering not yet run.")
        else:
            single_struct = aout.valid_struct
            plot_radial_displacement(
                aout.input_struct,
                single_struct[int(self.singles.get())],
                aout.new_label,
            )

    def activateCheck(self):
        use_disp = self.use_disp.get()
        if use_disp == 1:  # whenever checked
            self.max_disp.config(state=tk.NORMAL)
            self.max_disp.insert(tk.END, "2.0")
        elif use_disp == 0:  # whenever unchecked
            self.max_disp.delete(0, tk.END)
            self.max_disp.config(state=tk.DISABLED)

    def run_clustering(self):
        aout.input_struct = io.read(self.input.get())
        threading.Thread(
            target=cluster,
            args=(
                self.algorithm.get(),
                self.shift.get(),
                self.musym.get(),
                self.param.get(),
                self.output_dir.get(),
                aout,
                self.use_disp.get(),
                self.max_disp.get(),
            ),
            kwargs={"logger": f"{self.logger.name}.clustering"},
        ).start()

    def write_to_cif(self):
        output_cif = tk.filedialog.asksaveasfilename(defaultextension=".cif")

        try:
            os.remove(output_cif)
        except Exception:
            pass

        tmp_cif = os.path.join(os.path.dirname(self.input.get()), "tmp.cif")
        io.write(tmp_cif, aout.shifted)
        with open(output_cif, "w") as c2:
            with open(tmp_cif, "r") as c1:
                for line in c1:
                    c2.write(line.replace(aout.new_label, "H:mu"))
        os.remove(tmp_cif)
        self.logger.info("Saved muon sites as %s.", output_cif)

    def write_single_cif(self):
        init_name = (
            os.path.basename(self.input.get()).replace(".cif", "")
            + "_mu"
            + self.singles.get()
            + ".cif"
        )
        single_cif = tk.filedialog.asksaveasfilename(initialfile=init_name)

        try:
            os.remove(single_cif)
        except Exception:
            pass

        tmp_cif = os.path.join(os.path.dirname(self.input.get()), "tmp.cif")
        single_struct = aout.valid_struct
        io.write(tmp_cif, single_struct[int(self.singles.get())])
        with open(single_cif, "w") as c2:
            with open(tmp_cif, "r") as c1:
                for line in c1:
                    c2.write(line.replace(aout.new_label, "H:mu"))
        os.remove(tmp_cif)
        self.logger.info("Saved muon site %s as %s.", self.singles.get(), single_cif)

    def load_dir(self):
        results_folder = tk.filedialog.askdirectory()
        self.output_dir.delete(0, tk.END)
        self.output_dir.insert(tk.END, os.path.normpath(results_folder))

    def save_structures(self):
        pickle_file = self.input.get().replace(".cif", ".pkl")
        with open(pickle_file, "wb") as f:
            pickle.dump(aout, f)
        self.logger.info("Saved structures to %s.", pickle_file)

    def load_structures(self):
        pickle_file = self.input.get().replace(".cif", ".pkl")
        with open(pickle_file, "rb") as f:
            bout = pickle.load(f)
        aout.input_struct = bout.input_struct
        aout.new_label = bout.new_label
        aout.valid_struct = bout.valid_struct
        aout.muon_sites = bout.muon_sites
        aout.muon_sites_supercell = bout.muon_sites_supercell
        aout.shifted = bout.shifted
        aout.clust_composition = bout.clust_composition
        self.logger.info("Loaded structures from %s.", pickle_file)

    def list_sites(self):
        single_struct = aout.valid_struct
        min_energy = single_struct[0].get_potential_energy()
        for i in range(len(single_struct)):
            struct_energy = single_struct[i].get_potential_energy()
            rel_energy = struct_energy - min_energy
            mupos = aout.muon_sites[i]
            self.logger.info(
                "Muon site %i at %s has relative energy %s eV.",
                i,
                mupos,
                rel_energy,
            )
