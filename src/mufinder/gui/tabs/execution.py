import logging
import multiprocessing
import os
import pickle
import shutil
import subprocess
import threading
import tkinter as tk
import tkinter.messagebox
import tkinter.scrolledtext as tkst
import tkinter.ttk as ttk
from pathlib import Path
from platform import system

from mufinder.core.runtools.write_input import (
    cell2cif,
    create_site_tree,
    find_cell_files,
    get_param_text,
)
from mufinder.gui.config import castep_mpi, castep_serial, hpc_names, mpi_exec
from mufinder.gui.widgets.frames import disable_children, enable_children
from mufinder.gui.widgets.get_numbers import get_numbers
from mufinder.gui.widgets.multi_dialog import CustomDialog
from mufinder.gui.widgets.table import SimpleTableInput
from mufinder.gui.widgets.text_handler import TextHandler
from mufinder.gui.widgets.textfield import TextField
from mufinder.gui.widgets.tooltip import ToolTip
from mufinder.hpc.glamdring import addqueue
from mufinder.hpc.ssh import (
    check_connection,
    copy_queue,
    create_session,
    pull,
    search_queue,
    send_command,
    sftp,
    submit_job,
)
from mufinder.hpc.write_script import write_script

if system() == "Windows":
    use_wsl = True
else:
    use_wsl = False


class ExecutionTab(ttk.Notebook):
    def __init__(self, *args, **kwargs):
        # super().__init__(*args, **kwargs)
        ttk.Frame.__init__(self, *args, **kwargs)

        self.jobs = []
        self.sites = []
        self.rootname = []
        self.hpc_address = ""
        self.submission_sys = ""
        self.password = None
        self.session = None
        self.cores_per_node = 24
        self.use_custom_params = False

        # commands for alternative queueing systems
        self.custom_submit = ""
        self.custom_queue_check = ""
        self.custom_cancel = ""
        # This will be set to true if login name different to username
        self.use_login_name = False
        self.login_name = ""

        self.old_param = None
        self.custom_params = None
        self.custom_cell = ""
        self.castep_calc = None

        cells_lbl = tk.Label(self, text="Input files:")
        cells_lbl.grid(row=0, column=0, sticky=tk.W)
        self.cells = tk.Entry(self)
        self.cells.grid(row=1, column=0, sticky=tk.W)
        ToolTip(cells_lbl, "folder containing input cell files")
        ttk.Button(self, text="Load", width=10, command=self.load_dir).grid(
            row=2, column=0, sticky=tk.W
        )

        self.mode = tk.StringVar(self)
        mode_options = ["local"] + hpc_names
        tk.Label(self, text="Mode").grid(row=3, column=0, sticky=tk.W)
        w_mode = ttk.OptionMenu(self, self.mode, mode_options[0], *mode_options)
        w_mode.grid(row=4, column=0, sticky=tk.W)

        self.param_frame = tk.LabelFrame(self, text="Parameters")
        self.param_frame.grid(
            row=0,
            column=1,
            columnspan=1,
            rowspan=7,
            sticky="WE",
            padx=2,
            pady=2,
            ipadx=2,
            ipady=2,
        )

        self.charge = tk.StringVar(self)
        charge_options = ["mu+", "muonium","no muon"]
        tk.Label(self.param_frame, text="Muon charge state").grid(
            row=0, column=0, sticky=tk.W
        )
        w_charge = ttk.OptionMenu(
            self.param_frame, self.charge, charge_options[0], *charge_options
        )
        w_charge.grid(row=1, column=0, sticky=tk.W)

        self.xc = tk.StringVar(self)
        xc_options = ["LDA", "PBE"]
        tk.Label(self.param_frame, text="Exchange-correlation functional").grid(
            row=2, column=0, sticky=tk.W
        )
        w_xc = ttk.OptionMenu(self.param_frame, self.xc, xc_options[0], *xc_options)
        w_xc.grid(row=3, column=0, sticky=tk.W)

        self.spin_pol = tk.IntVar()
        tk.Checkbutton(
            self.param_frame, text="spin-polarized?", variable=self.spin_pol
        ).grid(row=4, column=0, sticky=tk.W)

        self.basis_prec = tk.StringVar(self)
        bp_options = ["COARSE", "MEDIUM", "FINE", "PRECISE", "EXTREME"]
        tk.Label(self.param_frame, text="Basis precision").grid(
            row=5, column=0, sticky=tk.W
        )
        w_bp = ttk.OptionMenu(
            self.param_frame, self.basis_prec, bp_options[2], *bp_options
        )
        w_bp.grid(row=6, column=0, sticky=tk.W)

        self.solver = tk.StringVar(self)
        solver_options = ["Density mixing", "edft"]
        tk.Label(self.param_frame, text="SCF Solver").grid(row=0, column=2, sticky=tk.W)
        w_bp = ttk.OptionMenu(
            self.param_frame, self.solver, solver_options[0], *solver_options
        )
        w_bp.grid(row=1, column=2, sticky=tk.W)

        k_label = tk.Label(self.param_frame, text="k-point grid")
        k_label.grid(row=2, column=2, sticky=tk.W)
        self.kpoints = SimpleTableInput(self.param_frame, 1, 3)
        self.kpoints.grid(row=3, column=2, sticky=tk.W)
        self.kpoints.set("")
        self.param_frame.grid_columnconfigure(1, weight=1)

        adv = tk.Label(self.param_frame, text="Advanced options")
        adv.grid(row=4, column=2, sticky=tk.W)

        self.custom_params_btn = ttk.Button(
            self.param_frame, text="Custom parameters", command=self.input_custom_params
        )
        self.custom_params_btn.grid(row=5, column=2, sticky=tk.W)

        self.reenable_gui_btn = ttk.Button(
            self.param_frame,
            text="Enable GUI",
            command=self.enable_param_frame,
            state="disabled",
        )
        self.reenable_gui_btn.grid(row=5, column=2, sticky=tk.E)

        self.cell_kw = ttk.Button(
            self.param_frame, text="Cell keywords", command=self.input_custom_cell
        )
        self.cell_kw.grid(row=6, column=2, sticky=tk.W)

        self.castep_comm = tk.StringVar(self)
        castep_options = ["castep.mpi", "castep.serial"]
        castep_comm_label = tk.Label(self, text="CASTEP command")
        castep_comm_label.grid(row=8, column=1, sticky=tk.W)
        ToolTip(
            castep_comm_label, "Choose between parallel or serial version of CASTEP"
        )
        w_castep = ttk.OptionMenu(
            self, self.castep_comm, castep_options[0], *castep_options
        )
        w_castep.grid(row=9, column=1, sticky=tk.W)

        self.inputnums = TextField(self, "sites to run", 5, 0, "", "Sites to run")
        run = ttk.Button(self, text="Run calculations", command=self.run_calc)
        run.grid(row=8, column=0, sticky=tk.W, pady=4)

        self.output_text = tkst.ScrolledText(self, height=10, width=100)
        self.output_text.grid(column=0, row=12, columnspan=2)

        self.logger = logging.getLogger("execution")
        self.logger.addHandler(TextHandler(self.output_text))
        self.logger.setLevel(logging.INFO)

        self.grid_rowconfigure(13, weight=1)

        cluster_frame = tk.LabelFrame(self, text="Manage jobs on HPC cluster")
        cluster_frame.grid(
            row=17, columnspan=2, sticky="WE", padx=2, pady=2, ipadx=2, ipady=2
        )

        run = ttk.Button(cluster_frame, text="Check queue", command=self.check_queue)
        run.grid(row=14, column=3, sticky=tk.W, pady=4)

        self.working_folder = TextField(
            cluster_frame,
            "Working directory",
            14,
            0,
            "",
            "Folder to run calculations from",
        )

        self.username = TextField(
            cluster_frame, "username", 16, 0, "", "HPC cluster username"
        )
        self.jobname = TextField(
            cluster_frame, "Job name", 14, 1, "job", "Jobs will appear as jobname"
        )
        self.num_cores = TextField(
            cluster_frame,
            "Number of processes",
            16,
            1,
            "24",
            "Number of processes to use",
        )

        self.walltime = TextField(
            cluster_frame,
            "walltime for job",
            18,
            1,
            "48:00:00",
            "Number of processes to use",
        )

        load_jobs = ttk.Button(cluster_frame, text="Load jobs", command=self.load_jobs)
        load_jobs.grid(row=19, column=3, sticky=tk.W, pady=4)

        self.jobid = TextField(
            cluster_frame, "Job ID", 16, 2, "", "Enter ID of job to cancel"
        )

        self.resub = tk.IntVar()
        tk.Checkbutton(
            cluster_frame, text="resubmit if not finished", variable=self.resub
        ).grid(row=15, column=3, sticky=tk.W)

        cancel = ttk.Button(
            cluster_frame, text="Cancel job(s)", command=self.cancel_job
        )
        cancel.grid(row=17, column=3, sticky=tk.W, pady=4)

        cancel_all = ttk.Button(
            cluster_frame, text="Cancel all", command=self.cancel_all
        )
        cancel_all.grid(row=18, column=2, sticky=tk.W, pady=4)

        cancel_local = ttk.Button(
            self, text="Cancel local calculation", command=self.cancel_loc
        )
        cancel_local.grid(row=9, column=0, sticky=tk.W, pady=4)

        ttk.Button(self, text="Quit", command=self.quit).grid(
            row=22, column=0, sticky=tk.W, pady=4
        )

    def get_login(self):
        if self.use_login_name:
            return self.login_name
        else:
            return self.username.get()

    def load_dir(self):
        cells_folder = tk.filedialog.askdirectory()
        self.cells.delete(0, "end")
        self.cells.insert(tk.END, os.path.normpath(cells_folder))

    def run_calc(self):
        if self.mode.get() == "local":
            thread = threading.Thread(target=self.run_local)
            thread.start()
        else:
            self.connect()
            thread = threading.Thread(target=self.run_cluster)
            thread.start()

    def run_local(self):
        sites = get_numbers(
            self.inputnums.get(), logger=f"{self.logger.name}.run_local"
        )
        cell_dir = Path(self.cells.get())
        cell_files = find_cell_files(cell_dir, sites)

        out_dir = cell_dir.parent / "results"
        seed_paths = create_site_tree(
            cell_files,
            out_dir,
            custom_params=(self.custom_params if self.use_custom_params else None),
            custom_cell=self.custom_cell,
            castep_params=self.castep_params(),
        )

        for site, seed_path in enumerate(seed_paths):
            command_args = []

            if use_wsl:
                command_args.append("wsl")

            if self.castep_comm.get() == "castep.mpi":
                version = subprocess.check_output(
                    [*command_args, mpi_exec, "--version"]
                )

                command_args.append(mpi_exec)

                if "Open MPI" in str(version):
                    command_args.append("--oversubscribe")

                command_args.extend(
                    [
                        "-n",
                        str(multiprocessing.cpu_count()),
                        castep_mpi,
                        str(seed_path[1]),
                    ]
                )
            else:
                command_args.extend([castep_serial, str(seed_path[1])])

            if seed_path[0] == 0:
                self.logger.info("Running calculation for cell relaxation.")
            else:
                self.logger.info("Running calculation for site %s." % seed_path[0])

            self.castep_calc = subprocess.Popen(
                command_args,
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,
            )

            exit_code = self.castep_calc.wait()

            self.logger.info(f"Calculation finished with exit code {exit_code}")

            try:
                output_cell = seed_path[1].with_name(seed_path[1].name + "-out.cell")
                castep_file = seed_path[1].with_suffix(".castep")

                if seed_path[0] == 0:
                    site_out_dir = out_dir / "nomuon"

                    if site_out_dir.is_dir():
                        shutil.rmtree(site_out_dir)
                    site_out_dir.mkdir(parents=True, exist_ok=False)

                else:
                    site_out_dir = out_dir

                shutil.copy(castep_file, site_out_dir / castep_file.name)
                shutil.copy(output_cell, site_out_dir / output_cell.name)

                if seed_path[0] == 0:
                    cell2cif(
                        site_out_dir / output_cell.name,
                        logger=f"{self.logger.name}.run_local",
                    )

                self.logger.info("Done!")

            except Exception:
                self.logger.info("Calculation for site %s incomplete." % site)

        self.sites.extend(sites)

    def get_password(self):
        if self.use_login_name:
            self.logger.info(
                "Attempting to connect to %s with login name %s."
                % (self.hpc_address, self.get_login()),
            )
        else:
            self.logger.info(
                "Attempting to connect to %s with username %s."
                % (self.hpc_address, self.get_login()),
            )

        while True:
            temp_pass = tk.simpledialog.askstring(
                "Password", "Enter %s password:" % self.mode.get(), show="*"
            )
            try:
                self.session = create_session(
                    self.hpc_address, self.get_login(), temp_pass
                )
                self.logger.info("Connection successful.")
            except Exception:
                self.logger.info("Connection unsuccessful.")
            return temp_pass

    def connect(self):
        if not check_connection(self.session):
            self.password = self.get_password()

    def run_cluster(self):
        job_ids = []

        sites = get_numbers(
            self.inputnums.get(), logger=f"{self.logger.name}.run_cluster"
        )

        cell_dir = Path(self.cells.get())
        cell_files = find_cell_files(cell_dir, sites)

        castep_params = self.castep_params()
        out_dir = cell_dir.parent / "results"

        seed_paths = create_site_tree(
            cell_files,
            out_dir,
            custom_params=(self.custom_params if self.use_custom_params else None),
            custom_cell=self.custom_cell,
            castep_params=castep_params,
        )

        self.rootname = os.path.basename(seed_paths[0][1]).replace(
            "_{}".format(seed_paths[0][0]), ""
        )

        cwd = self.working_folder.get()
        cdoutput, cderror = send_command(self.session, f"cd {cwd}")

        if "No such file or directory" in cderror:
            send_command(self.session, f"mkdir -p {cwd}")
            self.logger.info(f"Created working directory {cwd}.")
        else:
            self.logger.info(f"Working in {cwd}.")

        for site, seed_path in seed_paths:
            if self.submission_sys == "addqueue":
                self.custom_submit = addqueue(
                    self.jobname.get(), self.num_cores.get(), seed_path.name
                )

            else:
                write_script(
                    self.mode.get(),
                    self.submission_sys,
                    seed_path.parent,
                    self.jobname.get(),
                    self.num_cores.get(),
                    seed_path.name,
                    self.cores_per_node,
                    self.walltime.get(),
                )

            sftp(
                self.session,
                str(Path.as_posix(seed_path.parent)),
                cwd,
                recursive=True,
            )

            id_sdb = submit_job(
                self.session, self.submission_sys, cwd, site, self.custom_submit
            )

            if site == 0:
                self.logger.info("Submitted job %s for cell relaxation." % id_sdb)
            else:
                self.logger.info("Submitted job %s for Site %i." % (id_sdb, site))

            job_ids.append(id_sdb)

        self.jobs.extend(job_ids)
        self.sites.extend(sites)

        self.save_jobs_func()

    def clear_finished_jobs(self):
        while None in self.jobs:
            self.jobs.remove(None)

        while None in self.sites:
            self.sites.remove(None)

    def check_queue(self):
        self.connect()

        if not self.jobs:
            self.logger.info("No jobs running!")
            self.save_jobs_func()

            return None

        self.clear_finished_jobs()

        resub_jobs = []
        resub_sites = []
        queue_str = copy_queue(
            self.submission_sys,
            self.session,
            self.username.get(),
            self.custom_queue_check,
        )

        for i, job_id in enumerate(self.jobs):
            inqueue, job_status = search_queue(job_id, queue_str, self.submission_sys)

            if self.sites[i] == 0:
                site_name = "cell relaxation"
            else:
                site_name = "Site %i" % self.sites[i]

            if inqueue:
                if job_status == "waiting":
                    self.logger.info(f"Job {job_id} for {site_name} still in queue.")
                elif job_status == "running":
                    self.logger.info(f"Job {job_id} for {site_name} running.")
            else:
                results = os.path.join(os.path.dirname(self.cells.get()), "results")

                if not os.path.isdir(results):
                    os.mkdir(results)
                if self.sites[i] == 0:
                    results = os.path.join(results, "nomuon")
                    if not os.path.isdir(results):
                        os.mkdir(results)

                castep_file = "%s/site%i/%s_%i.castep" % (
                    self.working_folder.get(),
                    self.sites[i],
                    self.rootname,
                    self.sites[i],
                )

                outcell = castep_file.replace(".castep", "-out.cell")
                output, error = send_command(self.session, "ls %s" % outcell)

                if output.rstrip() == outcell:
                    self.logger.info("Job for %s finished." % site_name)
                    pull(
                        self.session,
                        castep_file,
                        Path(
                            os.path.join(results, os.path.basename(castep_file))
                        ).as_posix(),
                    )
                    pull(
                        self.session,
                        outcell,
                        Path(
                            os.path.join(results, os.path.basename(outcell))
                        ).as_posix(),
                    )
                    self.logger.info(f"Output files copied to {results}.")

                    # Create cif file for relaxed structure if
                    # performing cell relaxation
                    if self.sites[i] == 0:
                        cell2cif(
                            os.path.join(results, os.path.basename(outcell)),
                            logger=f"{self.logger.name}.check_queue",
                        )

                    self.jobs[i] = None
                    self.sites[i] = None
                else:
                    self.logger.info(
                        "Job %i for %s is no longer in queue, but did not finish."
                        % (self.jobs[i], site_name),
                    )
                    if self.resub.get():
                        id_sdb = submit_job(
                            self.session,
                            self.submission_sys,
                            self.working_folder.get(),
                            self.sites[i],
                            self.custom_submit,
                        )

                        self.logger.info(
                            "Submitted job %s for continuation of %s."
                            % (id_sdb, site_name),
                        )

                        tmp_site = self.sites[i]
                        self.jobs[i] = None
                        self.sites[i] = None
                        resub_jobs.append(id_sdb)
                        resub_sites.append(tmp_site)

        self.jobs.extend(resub_jobs)
        self.sites.extend(resub_sites)
        self.save_jobs_func()

    def castep_params(self):
        return {
            "charge": self.charge.get(),
            "xc": self.xc.get(),
            "spin_pol": self.spin_pol.get(),
            "basis_prec": self.basis_prec.get(),
            "solver": self.solver.get(),
            "kpoints": self.kpoints.get(),
        }

    def enable_custom_params(self):
        self.use_custom_params = True
        disable_children(self.param_frame)
        self.custom_params_btn.configure(state="normal")

        style = ttk.Style()
        style.configure("FG.TButton", foreground="green", background="white")
        self.custom_params_btn.configure(style="FG.TButton")

        self.reenable_gui_btn.configure(state="normal")
        self.cell_kw.configure(state="normal")

    def enable_param_frame(self):
        if tkinter.messagebox.askyesno(
            "Warning",
            "Re-enabling parameter GUI will reset custom parameters. "
            "Do you wish to proceed?",
        ):
            self.use_custom_params = False
            enable_children(self.param_frame)
            self.reenable_gui_btn.configure(state="disabled")

            style = ttk.Style()
            style.configure("FG.TButton", foreground="black", background="white")
            self.custom_params_btn.configure(style="FG.TButton")

        else:
            pass

    def input_custom_params(self):
        if not self.use_custom_params:
            self.custom_params = get_param_text(**dict({i:self.castep_params()[i] for
                        i in self.castep_params() if i!='kpoints'}))
            self.enable_custom_params()

        self.custom_params = CustomDialog(
            self, self.custom_params, "Custom parameters"
        ).show()

    def input_custom_cell(self):
        self.custom_cell = CustomDialog(self, self.custom_cell, "Cell keywords").show()

    def load_jobs(self):
        pickle_file = os.path.join(self.cells.get(), "jobs.pkl")
        with open(pickle_file, "rb") as f:
            self.sites = pickle.load(f)
            self.jobs = pickle.load(f)
            self.rootname = pickle.load(f)
            self.username.replace(pickle.load(f))
            self.working_folder.replace(pickle.load(f))
            self.jobname.replace(pickle.load(f))
            self.num_cores.replace(pickle.load(f))

        self.clear_finished_jobs()
        self.logger.info(f"Loaded job information from {pickle_file}.")

    def save_jobs_func(self):
        pickle_file = os.path.join(self.cells.get(), "jobs.pkl")
        self.clear_finished_jobs()

        with open(pickle_file, "wb") as f:
            pickle.dump(self.sites, f)
            pickle.dump(self.jobs, f)
            pickle.dump(self.rootname, f)
            pickle.dump(self.username.get(), f)
            pickle.dump(self.working_folder.get(), f)
            pickle.dump(self.jobname.get(), f)
            pickle.dump(self.num_cores.get(), f)

        self.logger.info(f"Saved job information to {pickle_file}.")

    def cancel_job(self):
        self.connect()

        job_ids = get_numbers(self.jobid.get(), logger=f"{self.logger.name}.cancel_job")

        if self.submission_sys == "slurm":
            cancel_comm = "scancel"
        elif self.submission_sys == "pbs":
            cancel_comm = "qdel"
        else:
            cancel_comm = self.custom_cancel

        for job_id in job_ids:
            send_command(
                self.session,
                "%s %s" % (cancel_comm, job_id),
            )

            for i, known_id in enumerate(self.jobs):
                if int(job_id) == known_id:
                    self.jobs.pop(i)
                    self.sites.pop(i)
                    self.logger.info("Cancelled job %s." % job_id)

        self.save_jobs_func()

    def cancel_all(self):
        self.connect()

        if self.submission_sys == "slurm":
            cancel_comm = "scancel"
        elif self.submission_sys == "pbs":
            cancel_comm = "qdel"
        else:
            cancel_comm = self.custom_cancel

        for i, job_id in enumerate(self.jobs):
            send_command(
                self.session,
                "%s %s" % (cancel_comm, job_id),
            )

        self.jobs = []
        self.sites = []
        self.logger.info("Cancelled all jobs.")
        self.save_jobs_func()

    def cancel_loc(self):
        if self.castep_calc is None:
            self.logger.info("No local calculations submitted!")
            return

        poll = self.castep_calc.poll()

        if poll is None:
            self.castep_calc.terminate()
            self.castep_calc.wait()
            self.logger.info("Cancelled running calculation.")
        else:
            self.logger.info("No calculations running locally.")
