import logging
import os
import threading
import tkinter as tk
import tkinter.scrolledtext as tkst
import tkinter.ttk as ttk

import numpy as np
from ase import io
from ase.build.supercells import make_supercell
from matplotlib import pyplot as plt

from mufinder.core.dipolar.calcfield import calc_field, calc_tensor, enter_pos, get_MUC
from mufinder.core.dipolar.mag_array import load_mag_array, write_mag_array
from mufinder.gui.config import aout, dipole_fields
from mufinder.gui.custom_view import custom_view
from mufinder.gui.widgets.get_numbers import get_numbers
from mufinder.gui.widgets.table import SimpleTableInput
from mufinder.gui.widgets.text_handler import TextHandler
from mufinder.gui.widgets.textfield import TextField


class DipoleFieldTab(ttk.Notebook):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.input = TextField(self, "Input structure:", 0, 0, "", None)
        ttk.Button(self, text="Load", width=10, command=self.set_struct).grid(
            row=2, column=0, sticky=tk.W
        )

        self.current_dist = 1
        self.distorted = tk.IntVar(value=self.current_dist)

        self.d = tk.Checkbutton(
            self, text="Use distorted structures?", variable=self.distorted
        )
        self.d.grid(row=4, column=0, sticky=tk.W)

        self.inputnums = TextField(
            self,
            "Muon sites to include",
            7,
            0,
            "",
            "Indices of sites for which to calculate field (as defined from analysis)",
        )

        mode_options = ["auto", "manual"]
        self.current_mode = "auto"
        self.mode = tk.StringVar(self)

        tk.Label(self, text="Mode").grid(row=5, column=0, sticky=tk.W)
        self.m = ttk.OptionMenu(self, self.mode, mode_options[0], *mode_options)
        self.m.grid(row=6, column=0, sticky=tk.W)

        self.find_equiv = tk.IntVar()
        self.fe = tk.Checkbutton(
            self, text="Find equivalent muon positions", variable=self.find_equiv
        )
        self.fe.grid(row=9, column=0, sticky=tk.W)
        ttk.Button(self, text="Run", command=self.calculate_fields).grid(
            row=13, column=0, sticky=tk.W, pady=4
        )

        self.output_text = tkst.ScrolledText(self, height=10, width=100)
        self.output_text.grid(column=0, row=14, columnspan=2)

        self.logger = logging.getLogger("dipolefield")
        self.logger.addHandler(TextHandler(self.output_text))
        self.logger.setLevel(logging.INFO)

        self.n = TextField(self, "Number of cells to use", 0, 1, "100", None)

        self.incomm = tk.IntVar()
        self.ic = tk.Checkbutton(
            self, text="Incommensurate magnetic structure?", variable=self.incomm
        )
        self.ic.grid(row=2, column=1, sticky=tk.W)

        k_label = tk.Label(self, text="Propagation vector")
        k_label.grid(row=3, column=1, sticky=tk.W)
        self.k = SimpleTableInput(self, 1, 3)
        self.k.grid(row=4, column=1, sticky=tk.W)

        ttk.Button(
            self, text="Generate mag_array file", command=self.gen_magarray
        ).grid(row=5, column=1, sticky=tk.W, pady=4)

        self.mag_array_name = TextField(self, "Name of mag_array file", 6, 1, "", None)

        ttk.Button(
            self, text="Check magnetic structure", command=self.view_mag_struct
        ).grid(row=8, column=1, sticky=tk.W, pady=4)

        self.property = tk.StringVar(self)
        mode_options = ["Magnitude", "Vector", "Dipolar tensor"]
        tk.Label(self, text="Property to calculate").grid(row=9, column=1, sticky=tk.W)
        self.p = ttk.OptionMenu(self, self.property, mode_options[0], *mode_options)
        self.p.grid(row=10, column=1, sticky=tk.W)

        self.lorentz = tk.IntVar(value=1)
        self.lor = tk.Checkbutton(
            self, text="Include Lorentz field", variable=self.lorentz
        )
        self.lor.grid(row=11, column=1, sticky=tk.W)

        self.shape_factor = TextField(
            self, "shape factor", 12, 1, 0, "shape factor for demagnetizing field"
        )
        self.grid_rowconfigure(15, weight=1)  # spacing to improve appearance

        self.field_dist = TextField(
            self, "Data file for field distribution", 16, 0, "", None
        )

        ttk.Button(self, text="View plot", command=self.view_plot).grid(
            row=18, column=0, sticky=tk.W, pady=4
        )

        self.grid_rowconfigure(19, weight=1)  # spacing to improve appearance

        ttk.Button(self, text="Quit", command=self.quit).grid(
            row=20, column=0, sticky=tk.W, pady=4
        )

        self.mode_update()

    def mode_update(self):
        if self.mode.get() != self.current_mode:
            if self.mode.get() == "manual":
                self.m["menu"].invoke(1)
                self.inputnums.make_inactive()
                self.distorted.set(0)
                self.d.config(state=tk.DISABLED)
            else:
                self.inputnums.make_active()
                self.d.config(state=tk.NORMAL)
            self.current_mode = self.mode.get()

        if self.distorted.get() != self.current_dist:
            if self.distorted.get():
                self.fe.config(state=tk.DISABLED)
                self.find_equiv.set(0)
                self.ic.config(state=tk.DISABLED)
                self.incomm.set(0)
            else:
                self.fe.config(state=tk.NORMAL)
                self.ic.config(state=tk.NORMAL)
            self.current_dist = self.distorted.get()
        self.after(1000, func=self.mode_update)

    def set_struct(self):
        try:
            input_struct = tk.filedialog.askopenfilename()
            if input_struct != "":

                self.input.replace(os.path.normpath(input_struct))
                if ".cif" in str.lower(input_struct):
                    file_type = "CIF"
                    aout.input_struct = io.read(input_struct)
                elif "cell" in str.lower(input_struct):
                    file_type = "CELL"
                    aout.input_struct = make_supercell(
                        io.read(input_struct), np.identity(3)
                    )
                    self.logger.info(
                        (
                            "WARNING! Dipole field calculations are not currently"
                            " compatible with CASTEP CELL files."
                        )
                    )

                self.logger.info(f"{input_struct} loaded as input {file_type} file.")

                try:
                    custom_view(aout.input_struct)
                except UnicodeDecodeError:
                    pass
        except Exception:
            self.logger.error("Error loading file {input_struct}.  Please try again.")

    def calculate_fields(self):
        if self.mode.get() == "auto":
            indices = get_numbers(
                self.inputnums.get(), logger=f"{self.logger.name}.calculate_fields"
            )
            mupos = None
        else:
            mupos, indices = enter_pos(logger=f"{self.logger.name}.enter_pos")

        options = [
            self.incomm.get(),
            self.distorted.get(),
            self.mode.get(),
            self.find_equiv.get(),
            self.property.get() == "Vector",
            self.lorentz.get(),
        ]

        if self.property.get() in ["Magnitude", "Vector"]:
            threading.Thread(
                target=calc_field,
                args=(
                    aout,
                    options,
                    indices,
                    mupos,
                    self.input.get(),
                    1,
                    self.k.get(),
                    dipole_fields,
                    int(self.n.get()),
                    self.mag_array_name.get(),
                    self.field_dist,
                    float(self.shape_factor.get()),
                ),
                kwargs={"logger": f"{self.logger.name}.calc_field"},
            ).start()
        elif self.property.get() == "Dipolar tensor":
            threading.Thread(
                target=calc_tensor,
                args=(
                    aout,
                    options,
                    indices,
                    mupos,
                    self.input.get(),
                    1,
                    self.k.get(),
                    int(self.n.get()),
                    self.mag_array_name.get(),
                ),
                kwargs={"logger": f"{self.logger.name}.calc_tensor"},
            ).start()

    def gen_magarray(self):
        write_mag_array(self.input.get(), 1, logger=f"{self.logger.name}.gen_magarray")
        self.mag_array_name.replace(self.input.get().replace(".cif", ".mag_array"))

    def view_mag_struct(self):
        struct = io.read(self.input.get())
        mag_array = load_mag_array(self.mag_array_name.get(), 1)
        custom_view(get_MUC(self.incomm.get(), struct, 1, self.k.get(), mag_array))

    def view_plot(self):
        data = np.loadtxt(open(self.field_dist.get(), "rb"), delimiter=",")
        plt.plot(data[:, 0], data[:, 1])
        plt.ylabel("P(B)")
        plt.xlabel("B[T]")
        plt.ticklabel_format(style="sci", axis="x", scilimits=(0, 0))
        plt.show()
