"""Variables accessible across all GUI tabs."""

from configobj import ConfigObj

from mufinder.containers import mu_gen, mu_struct
from mufinder.data import CONFIG_PATH

gen = mu_gen()
aout = mu_struct()

final_struct = [None]
muons_for_fields = []
muons_for_fields2 = []
index_for_fields = []
dipole_fields = [None]
current_code = "CASTEP"
current_hpc = "local"

# read information from config file which is editable by the user

cfg = ConfigObj(str(CONFIG_PATH))

# get full path of mpirun executable
mpi_exec = cfg["MPI"]["mpi_loc"]

# get paths of CASTEP executables
castep_serial = cfg["CASTEP"]["castep_serial_loc"]
castep_mpi = cfg["CASTEP"]["castep_mpi_loc"]

hpc_options = cfg["HPC"]

hpc_names = [cluster for cluster in cfg["HPC"]]
