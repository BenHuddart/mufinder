import tkinter as tk
import tkinter.scrolledtext as tkst
import tkinter.ttk as ttk


class CustomDialog(tk.Toplevel):
    def __init__(self, parent, default, txt):
        super().__init__(parent)

        self.label = tk.Label(self, text=txt)
        self.entry = tkst.ScrolledText(self, height=5)
        self.ok_button = ttk.Button(self, text="Save", command=self.on_ok)

        self.label.pack(side="top", fill="x")
        self.entry.pack(side="top", fill="x")
        self.ok_button.pack(side="right")

        self.entry.insert("end-1c", default)
        self.entry.see("end")
        self.var = self.entry.get(0.0, tk.END)

    def on_ok(self, event=None):
        self.var = self.entry.get(0.0, tk.END)
        self.destroy()

    def show(self):
        self.wm_deiconify()
        self.entry.focus_force()
        self.wait_window()
        return self.var
