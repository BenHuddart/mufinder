import tkinter as tk

from mufinder.gui.widgets.tooltip import ToolTip


class TextField(tk.Frame):
    def __init__(self, page, text, R, C, default, tip, sticky=tk.W):
        super().__init__()

        self.label = tk.Label(page, text=text)
        self.label.grid(row=R, column=C, sticky=sticky)
        self.val = tk.Entry(page)
        self.val.insert(tk.END, default)
        self.val.grid(row=R + 1, column=C, sticky=sticky)

        if tip is not None:
            self.ttp = ToolTip(self.label, tip)

    def get(self):
        return self.val.get()

    def insert(self, value):
        self.val.insert(tk.END, value)

    def replace(self, value):
        self.val.delete(0, tk.END)
        self.val.insert(tk.END, value)

    def change_label(self, labelText, tooltip):
        self.label.config(text=labelText)
        self.ttp.text = tooltip

    def make_inactive(self):
        self.val.config(state=tk.DISABLED, highlightbackground="gray")

    def make_active(self):
        self.val.config(state=tk.NORMAL, highlightbackground="white")
