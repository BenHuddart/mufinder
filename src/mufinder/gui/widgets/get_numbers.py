import logging


def get_numbers(numbers, logger=None):
    logger = logging.getLogger(logger)

    selection = set()
    invalid = set()

    # tokens are comma separated values
    tokens = [x.strip() for x in numbers.split(",")]

    for i in tokens:
        if len(i) > 0:
            if i[:1] == "<":
                i = "1-%s" % (i[1:])
        try:
            # typically tokens are plain old integers
            selection.add(int(i))
        except ValueError:
            # if not, then it might be a range
            try:
                token = [int(k.strip()) for k in i.split("-")]
                if len(token) > 1:
                    token.sort()
                    # we have items seperated by a dash
                    # try to build a valid range
                    first = token[0]
                    last = token[-1]
                    for x in range(first, last + 1):
                        selection.add(x)
            except Exception:
                # not an int and not a range...
                invalid.add(i)

    # Report invalid tokens before returning valid selection
    if len(invalid) > 0:
        logger.error("Invalid set: " + str(invalid))

    return list(selection)
