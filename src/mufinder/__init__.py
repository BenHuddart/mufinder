import tkinter as tk
import tkinter.ttk as ttk
from functools import partial
from platform import system

import matplotlib

from mufinder.data import get_icon
from mufinder.gui.config import current_hpc, gen, hpc_options
from mufinder.gui.tabs.analysis import AnalysisTab
from mufinder.gui.tabs.dipolefield import DipoleFieldTab
from mufinder.gui.tabs.execution import ExecutionTab
from mufinder.gui.tabs.generation import GenerationTab


def partial_f(f, *args, **kwargs):
    f_p = partial(f, *args, **kwargs)
    f_p.__name__ = f.__name__
    return f_p


def hpc_update(root, execute_tab):
    global current_hpc

    if execute_tab.mode.get() == "local":
        execute_tab.hpc_address = ""
        if execute_tab.mode.get() != current_hpc:
            execute_tab.working_folder.replace("")
            execute_tab.username.replace("")
    else:
        if execute_tab.mode.get() != current_hpc:
            if execute_tab.session is not None:
                execute_tab.session.close()
            execute_tab.hpc_address = hpc_options[execute_tab.mode.get()]["address"]
            execute_tab.submission_sys = hpc_options[execute_tab.mode.get()][
                "queue_system"
            ].casefold()

            if execute_tab.submission_sys.lower() != 'slurm' and 'pbs':
                execute_tab.custom_submit = hpc_options[execute_tab.mode.get()][
                    "custom_submit"]
                execute_tab.custom_queue_check = hpc_options[execute_tab.mode.get()][
                    "custom_queue_check"]
                execute_tab.custom_cancel = hpc_options[execute_tab.mode.get()][
                    "custom_cancel"]
            execute_tab.hpc_address = hpc_options[execute_tab.mode.get()]["address"]
            execute_tab.working_folder.replace(
                hpc_options[execute_tab.mode.get()]["working_dir"]
            )
            execute_tab.username.replace(
                hpc_options[execute_tab.mode.get()]["username"]
            )
            try:
                execute_tab.login_name = hpc_options[execute_tab.mode.get()][
                    "login_name"
                ]
                execute_tab.use_login_name = True
            except Exception:
                pass
            execute_tab.cores_per_node = hpc_options[execute_tab.mode.get()][
                "cores_per_node"
            ]
            execute_tab.walltime.replace(
                hpc_options[execute_tab.mode.get()]["walltime"]
            )

    current_hpc = execute_tab.mode.get()
    root.after(1000, func=partial_f(hpc_update, root, execute_tab))


def counter(root, gen_tab):

    count = len(gen.site_list)

    if gen_tab.count.get() != count:
        gen_tab.count.replace(count)

    root.after(500, func=partial_f(counter, root, gen_tab))


def run_gui():
    root = tk.Tk()
    root.title("MuFinder")
    root.resizable(False, False)

    # set icon for Windows
    if system() == "Windows":
        root.iconbitmap(get_icon("mufinder_logo.ico"))

    # set icon for Linux
    if system() == "Linux":
        icon = tk.PhotoImage(file=get_icon("mufinder_logo.gif"))
        root.tk.call("wm", "iconphoto", root._w, icon)

    # Set matplotlib backend
    matplotlib.use("TkAgg")

    nb = ttk.Notebook(root)
    page1 = GenerationTab(nb)
    page2 = ExecutionTab(nb)
    page3 = AnalysisTab(nb)
    page4 = DipoleFieldTab(nb)

    nb.add(page1, text="Generation")
    nb.add(page2, text="Run")
    nb.add(page3, text="Analysis")
    nb.add(page4, text="Dipole Field")
    nb.grid(row=1, column=1, sticky="E", padx=5, pady=5, ipadx=5, ipady=5)

    root.after(0, func=partial_f(counter, root, page1))
    root.after(0, func=partial_f(hpc_update, root, page2))

    # There is currently a bug with matplotlib objects (such as ASE GUI)
    # in tkinter that results in the GUI crashing when scrolling while
    # over the object. This workaround catches the exception and prevents
    # the crash.
    while True:
        try:
            root.mainloop()
            break
        except UnicodeDecodeError:
            pass
