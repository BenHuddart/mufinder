import sys
from pathlib import Path

if getattr(sys, "frozen", False) and hasattr(sys, "_MEIPASS"):
    DATA_PATH = Path(sys.executable).parent
else:
    DATA_PATH = Path(__file__).absolute().parent

TEMPLATE_PATH = DATA_PATH / "templates"
ICONS_PATH = DATA_PATH / "icons"
CONFIG_PATH = DATA_PATH / "mufinder.ini"


def load_template(name):
    return (TEMPLATE_PATH / name).read_text()


def get_icon(name):
    return str(ICONS_PATH / name)
